// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 2102;
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var path = require('path');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var expressLayouts = require('express-ejs-layouts');
//set up cho upload file
var multipart  = require('connect-multiparty');
var multipartMiddleware = multipart();
app.use(bodyParser.urlencoded({
    extended: true
}));

// Thư mục chứa ảnh upload trên server

//end setup cho upload file

var configDB = require('./config/database.js');

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration
// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', path.join(__dirname, 'views'));
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.set("layout extractScripts", true);
app.use(expressLayouts);
app.set('layout', 'frontend/index');
app.locals.base = "http://ncsb.topicanative.edu.vn/";
app.locals.sologan = "3rd March 2017 - <b>FAMILY</b>: What does your family do at the weekends?";
// required for passport
app.use(session({
    secret: 'ilovescotchscotchyscotchscotch', // session secret
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
require('./app/backend/dashboard.js')(app, passport);
require('./app/backend/day.js')(app, passport);
require('./app/backend/user.js')(app, passport);
require('./app/backend/phonetics.js')(app, passport,multipartMiddleware);
require('./app/backend/grammar.js')(app, passport,multipartMiddleware);
require('./app/backend/vocabulary.js')(app, passport,multipartMiddleware);
// require('./app/home.js')(app, passport);

//route frontend
require('./app/frontend/phonetics.js')(app, passport);
require('./app/frontend/home.js')(app, passport);
require('./app/frontend/grammars.js')(app, passport);
require('./app/frontend/exercise.js')(app, passport);
require('./app/frontend/vocabulary.js')(app, passport);
require('./app/frontend/ajax.js')(app, passport);

// launch ======================================================================

app.listen(port);
console.log('The magic happens on port ' + port);
