$(document).ready(function() {
	var flag = 0;

	$(document).on("click",".phonetic_exercise",function() {
		var t = $(this);
		var id = t.attr("data-id")
		var href = t.attr('href')
		var content = $(href).find('.phonetic-wrapper').html()
		if(content.length == 0){
			$.ajax({
	            type: "POST",
	            url: "http://ncsb.topicanative.edu.vn/exercise/action",
	            data: {type: 'phonetic',idphonetic:id},
	            success: function(result){
	            	var dataReturn = JSON.parse(result)
	            	var index = 1
	            	var append = ''
	            	for (var i = 0; i < dataReturn.length; i++) {
						var content = JSON.parse(dataReturn[i].content);
						append += '<div class="question question--audio">'
						append += '<div class="question__info">'
						append += '<span class="question__number">'+index+'</span>'
						append += '<p class="question__title">'+content.content+'</p>'
						append += '</div>'
						append += '<div class="question__content">'
						append += '<div class="question__answer">'
						append += '<p>'
						append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="A" alt="">'
						append += '<label>A. '+content.option1+'</label>'
						append += '</p>'
						append += '<p>'
						append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="B" alt="">'
						append += '<label>B. '+content.option2+'</label>'
						append += '</p>'
						if(content.option3 != ''){
						append += '<p>'
						append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="C" alt="">'
						append += '<label>C. '+content.option3+'</label>'
						append += '</p>'
						}
						if(content.option4 != ''){
						append += '<p>'
						append += '	<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="D" alt="">'
						append += '<label>D. '+content.option4+'</label>'
						append += '</p>'
						}
						append += '</div>'
						append += '<div class="question__data">'
						append += '<a class="audio fa fa-volume-up" onclick="this.firstElementChild.play();" data-behaviour="ga-event" data-value="Pronunciation audio" title="Bấm để nghe âm thanh">'
						append += '<audio src="http://ncsb.topicanative.edu.vn/uploads/phonetics/audio/'+dataReturn[i]._id+'.mp3">'
						append += '</audio>'
						append += '</a>'
						append += '</div>'
						append += '<div class="hint">'
						append += '</div>'
						append += '</div>'
						append += '</div>'
						index++; 
					}
					$(href).find('.phonetic-wrapper').html(append)
					$(href).find('.phonetic-wrapper').css({
						overflow: 'hidden',
						height: 'auto'
					});
	            },
	            async: false
	        });
		}
	});

	$(document).on("click",".all_voca",function() {
		var t = $(this);
		var idweek = t.attr('data-id');
		console.log(idweek)
		$.ajax({
	        type: "POST",
	        url: "http://ncsb.topicanative.edu.vn/exercise/action",
	        data: {type: 'week',idweek:idweek},
	        success: function(result){
				var dataReturn = JSON.parse(result)
				var append = ''
				var week_day = ['MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY','SUNDAY']
            	for (var i = 0; i < dataReturn.length; i++) {
            		append += '<li class="'
            		if (i==0) { 
            		append += 'active'
            		}
            		append += '"><a href="#hTab__content-'
            		if(i < 9){
            		append += '0'+(i+1)
            		}else{
            		append += (i+1)
            		}
            		append +='" data-toggle="tab" data-id="'+dataReturn[i].day+'" class="week_voca">'
            		append += week_day[i]+'</a></li>'
	            }
	            $('.dayofweek').html(append)
	        },
	        async: false
	    });
	});

	$(document).on("click",".week_voca",function() {
		var t = $(this)
		var week_day = t.attr('data-id');
		id = t.attr('href');
		$.ajax({
            type: "POST",
            url: "http://ncsb.topicanative.edu.vn/exercise/action",
            data: {type: 'voca_week',day:week_day},
            success: function(result){
            	var index = 1
            	var append = ''
            	var dataReturn = JSON.parse(result)
            	for (var i = 0; i < dataReturn.length; i++) {
            		append += '<li><a class="voca_id" data-id="'+dataReturn[i]._id+'" href="#tab__content-'+week_day+'-'+i+'" data-toggle="tab">'+dataReturn[i].keyword+'</a></li>'
            	}
            	$(id).find('.tab__navi').html(append)
            	var append2 = ''
            	for (var i = 0; i < dataReturn.length; i++) {
            		append2 += '<div class="tab__content '
            		if(i == 0){
            		append2 += 'active'
            		}
            		append2 += '" id="tab__content-'+week_day+'-'+i+'">'
            		append2 += '<div class="vocab-wrapper">'
            		append2 += '</div>'
            		append2 += '</div>'
            	}
            	$(id).find('.content_last').html(append2)
            	$.ajax({
		            type: "POST",
		            url: "http://ncsb.topicanative.edu.vn/exercise/action",
		            data: {type: 'vocabulary',idvocabulary:dataReturn[0]._id},
		            success: function(result){
		            	var dataReturn = JSON.parse(result)
		            	var index = 1
		            	var append = ''
		            	for (var i = 0; i < dataReturn.length; i++) {
		            		var content = JSON.parse(dataReturn[i].content);
		            		append += '<div class="question">'
							append += '<div class="question__info">'
							append += '<span class="question__number">'+index+'</span>'
							append += '<p class="question__title">'+content.content+'</p>'
							append += '</div>'
							append += '<div class="question__content">'
							append += '<div class="question__answer">'
							append += '<div class="row">'
							append += '<p class="col-md-6 col-sm-6 col-xs-6">'
							append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="A" alt="">'
							append += '<label for="ans-'+index+'1">A. '+content.option1+'</label>'
							append += '</p>'
							append += '<p class="col-md-6 col-sm-6 col-xs-6">'
							append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="B" alt="">'
							append += '<label for="ans-'+index+'2">B. '+content.option2+'</label>'
							append += '</p>'
							append += '</div>'
							append += '<div class="row">'
							if (content.option3 != '') {
							append += '<p class="col-md-6 col-sm-6 col-xs-6">'
							append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="C" alt="">'
							append += '<label for="ans-'+index+'3">C. '+content.option3+'</label>'
							append += '</p>'
							}
							if (content.option4 != '') {
							append += '<p class="col-md-6 col-sm-6 col-xs-6">'
							append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="D" alt="">'
							append += '<label for="ans-'+index+'4">D. '+content.option4+'</label>'
							append += '</p>'
							}
							append += '</div>'
							append += '<div style="clear: both;"></div>'
							append += '<span class="btn btn-primary btn-md show1">'
							append += 'Xem đáp án</span>'
							append += '<div class="hint"></div></div></div></div>'
						}
						$('#tab__content-'+week_day+'-0').find('.vocab-wrapper').html(append)
		            },
		            async: false
		        });
            },
            async: false
        });
	});

	$(document).on("click",".grammar_exercise",function() {
		var t = $(this);
		var id = t.attr("data-id")
		var href = t.attr('href')
		var content = $(href).find('.question').html()
		if(content.length == 0){
			$.ajax({
	            type: "POST",
	            url: "http://ncsb.topicanative.edu.vn/exercise/action",
	            data: {type: 'grammar',idgrammar:id},
	            success: function(result){
	            	var dataReturn = JSON.parse(result)
	            	var index = 1
	            	var append = ''
	            	for (var i = 0; i < dataReturn.length; i++) {
						var content = JSON.parse(dataReturn[i].content);
						append += '<div class="question"><div class="question__info">'
						append += '<span class="question__number">'+index+'</span>'
						append += '<p class="question__title">'+content.option+'</p></div><div class="question__content"><div class="question__answer"><div class="row"><p class="col-md-6 col-sm-6 col-xs-6">'
						append += '<input class="snow" id="ans-'+index+'1" name="'+dataReturn[i]._id+'" type="radio" value="A" alt="">'
						append += '<label for="ans-'+index+'1">A. '+content.option1+'</label></p>'
						append += '<p class="col-md-6 col-sm-6 col-xs-6">'
						append += '<input class="snow" id="ans-'+index+'2" name="'+dataReturn[i]._id+'" type="radio" value="B" alt="">'
						append += '<label for="ans-'+index+'2">B. '+content.option2+'</label></p>'
						append += '</div><div class="row">'
						if (content.option3 != '') {
						append += '<p class="col-md-6 col-sm-6 col-xs-6"><input class="snow" id="ans-'+index+'3" name="'+dataReturn[i]._id+'" type="radio" value="C" alt=""><label for="ans-'+index+'3">C. '+content.option3+'</label></p>'
						}
						if (content.option4 != '') {
						append += '<p class="col-md-6 col-sm-6 col-xs-6"><input class="snow" id="ans-'+index+'4" name="'+dataReturn[i]._id+'" type="radio" value="D" alt=""><label for="ans'+index+'4">D. '+content.option4+'</label></p>'
						}
						append += '</div><div style="clear: both;"></div><span class="btn btn-primary btn-md show1">Xem đáp án</span><div class="hint"></div></div></div></div>'
						index++; 
					}
					$(href).find('.question').html(append)
	            },
	            async: false
	        });
		}
	});

	$(document).on("click",".voca_id",function() {
		var t = $(this)
		var voca_id = t.attr('data-id');
		var id = t.attr('href');
		var content = $(id).find('.vocab-wrapper').html()
		console.log(content.length);
		if(content.length < 10){
			$.ajax({
	            type: "POST",
	            url: "http://ncsb.topicanative.edu.vn/exercise/action",
	            data: {type: 'vocabulary',idvocabulary:voca_id},
	            success: function(result){
	            	var dataReturn = JSON.parse(result)
	            	var index = 1
	            	var append = ''
	            	for (var i = 0; i < dataReturn.length; i++) {
	            		var content = JSON.parse(dataReturn[i].content);
	            		append += '<div class="question">'
						append += '<div class="question__info">'
						append += '<span class="question__number">'+index+'</span>'
						append += '<p class="question__title">'+content.content+'</p>'
						append += '</div>'
						append += '<div class="question__content">'
						append += '<div class="question__answer">'
						append += '<div class="row">'
						append += '<p class="col-md-6 col-sm-6 col-xs-6">'
						append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="A" alt="">'
						append += '<label for="ans-'+index+'1">A. '+content.option1+'</label>'
						append += '</p>'
						append += '<p class="col-md-6 col-sm-6 col-xs-6">'
						append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="B" alt="">'
						append += '<label for="ans-'+index+'2">B. '+content.option2+'</label>'
						append += '</p>'
						append += '</div>'
						append += '<div class="row">'
						if (content.option3 != '') {
						append += '<p class="col-md-6 col-sm-6 col-xs-6">'
						append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="C" alt="">'
						append += '<label for="ans-'+index+'3">C. '+content.option3+'</label>'
						append += '</p>'
						}
						if (content.option4 != '') {
						append += '<p class="col-md-6 col-sm-6 col-xs-6">'
						append += '<input class="snow" name="'+dataReturn[i]._id+'" type="radio" value="D" alt="">'
						append += '<label for="ans-'+index+'4">D. '+content.option4+'</label>'
						append += '</p>'
						}
						append += '</div>'
						append += '<div style="clear: both;"></div>'
						append += '<span class="btn btn-primary btn-md show1">'
						append += 'Xem đáp án</span>'
						append += '<div class="hint"></div></div></div></div>'
					}
					$(id).find('.vocab-wrapper').html(append)
	            },
	            async: false
	        });
		}
	});

	$('.anhlh2').click(function(event) {
		var div = document.getElementById("playerID");
		for (var i = 0; i < div.getElementsByTagName("iframe").length; i++) {
			var iframe = div.getElementsByTagName("iframe")[i].contentWindow;
		    func = 'pauseVideo'
		    iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
		}  
	});


	$('.week_day').click(function(event) {
		var t = $(this);
		var week_day = t.attr('data-id')
		var href = t.attr('href')
		var content = $(href).html().length
		if(content == 158){
			$.ajax({
	            type: "POST",
	            url: "exercise/action",
	            data: {type: 'day_one',day:week_day},
	            success: function(result){
	            	date = week_day
	            	var dataReturn = JSON.parse(result)
	            	var append = '<div class="question">'
	            	var index = 1
	            	for (var i = 0; i < dataReturn.length; i++) {
	            		var content = JSON.parse(dataReturn[i].content);
	            		if(dataReturn[i].type == 'phonetics'){
	            			append += '<div class="question question--audio"><div class="question__info"><span class="question__number">'+index+'</span><p class="question__title">'+content.content+'</p></div><div class="question__content"><div class="question__answer"></p><input name="'+dataReturn[i]._id+'" type="radio" value="A" alt=""><label>A. '+content.option1+'</label></p><p><input name="'+dataReturn[i]._id+'" type="radio" value="B" alt=""><label>B. '+content.option2+'</label></p>'
							if(content.option3 != ''){
								append += '<p><input name="'+dataReturn[i]._id+'" type="radio" value="C" alt=""><label>C. '+content.option3+'</label></p>'
							}
							if(content.option3 != ''){
								append += '<p><input name="'+dataReturn[i]._id+'" type="radio" value="D" alt=""><label>D. '+content.option4+'</label></p>'
							}		
							append += '</div><div class="question__data"><a class="audio fa fa-volume-up" onclick="this.firstElementChild.play();" data-behaviour="ga-event" data-value="Pronunciation audio" title="Bấm để nghe âm thanh"><audio src="http://ncsb.topicanative.edu.vn/uploads/phonetics/audio/'+dataReturn[i]._id+'.mp3"></audio></a></div><div class="hint"></div></div></div>'
	            		}
	            		if(dataReturn[i].type == 'grammar'){
	            			append += '<div class="question"><div class="question__info"><span class="question__number">'+index+'</span><p class="question__title">'+content.option+'</p></div><div class="question__content"><div class="question__answer"><div class="row"><p class="col-md-6 col-sm-6 col-xs-6"><input name="'+dataReturn[i]._id+'" type="radio" value="A" alt=""><label>A. '+content.option1+'</label></p><p class="col-md-6 col-sm-6 col-xs-6"><input name="'+dataReturn[i]._id+'" type="radio" value="B" alt=""><label>B. '+content.option2+'</label></p></div><div class="row">'
							if(content.option3 != ''){
							append += '<p class="col-md-6 col-sm-6 col-xs-6"><input name="'+dataReturn[i]._id+'" type="radio" value="C" alt=""><label>C. '+content.option3+'</label></p>'
							}
							if(content.option4 != ''){
							append += '<p class="col-md-6 col-sm-6 col-xs-6"><input name="'+dataReturn[i]._id+'" type="radio" value="D" alt=""><label>D. '+content.option4+'</label></p>'
							}
							append += '</div><div class="hint"></div></div></div></div>'
	            		}
	            		if(dataReturn[i].type == 'voca4'){
							append += '<div class="question"><div class="question__info"><span class="question__number">'+index+'</span><p class="question__title">'+content.content+'</p></div><div class="question__content"><div class="question__answer"><div class="row"><p class="col-md-6 col-sm-6 col-xs-6"><input name="'+dataReturn[i]._id+'" type="radio" value="A" alt=""><label>A. '+content.option1+'</label></p><p class="col-md-6 col-sm-6 col-xs-6"><input name="'+dataReturn[i]._id+'" type="radio" value="B" alt=""><label>B. '+content.option2+'</label></p></div><div class="row">'
							if(content.option3 != ''){
							append += '<p class="col-md-6 col-sm-6 col-xs-6"><input name="'+dataReturn[i]._id+'" type="radio" value="C" alt=""><label>C. '+content.option3+'</label></p>'
							}
							if(content.option4 != ''){
							append += '<p class="col-md-6 col-sm-6 col-xs-6"><input name="'+dataReturn[i]._id+'" type="radio" value="D" alt=""><label>D. '+content.option4+'</label></p>'
							}
							append += '</div><div class="hint"></div></div></div></div>'
	            		}
	            		index ++
	            	}
	            	append += '</div>'
	            	$(href).html(append)
	            },
	            async: false
	        });
		}
	});
	$(document).on("change","input:not(.snow)",function() {
    	if(flag == 0){
    		var t = $(this);
	    	var val = $(this).val();
	    	var name = $(this).attr('name');
			$.ajax({
	            type: "POST",
	            url: "http://ncsb.topicanative.edu.vn/exercise.html",
	            data: {'name': name,'val': val,date:date},
	            success: function(result){
	            	console.log(result)
	                t.parent().parent().parent().find('.hint').html(result)
	            },
	            async: false
	        });
    	}	
	});

	$(document).on("click",".show1",function() {
		$(this).next().slideDown('slow');
	});

	$(document).on("change","input.snow",function() {
    	if(flag == 0){
    		var t = $(this);
	    	var val = $(this).val();
	    	var name = $(this).attr('name');
			$.ajax({
	            type: "POST",
	            url: "http://ncsb.topicanative.edu.vn/exercise/action",
	            data: {type: 'check','val': val,name:name},
	            success: function(result){
	            	console.log(result)
	                t.parent().parent().parent().find('.hint').html(result)
	                //$('.hint').show();
	            },
	            async: false
	        });
    	}	
	});


	$('#nopbai').click(function(event) {
		$.ajax({
            type: "POST",
            url: "http://ncsb.topicanative.edu.vn/exercise",
            data: {date:date},
            success: function(result){
            	alert("Bạn làm đúng "+result+ " trên tổng số 15 câu !")
            	flag = 1;
				$('.hint').css('display', 'block');
				$('#nopbai').hide();
            },
            async: false
        });		
	});
});