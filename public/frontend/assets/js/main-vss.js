 
"use strict";

jQuery(document).ready(function() {

	/*_____________ scroll button_____________*/

	if (jQuery('.scroll_btn').length) {
		jQuery('.scroll_btn').each(function(){
			jQuery(this).on('click',function() {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = jQuery(this.hash);
					target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						jQuery('html, body').animate({
							scrollTop: target.offset().top
						}, 1000);
						return false;
					}
				}
			});
		});
	}
	
	/*_____________ match height _____________*/

	if(jQuery('.ul-mh').length){
		jQuery('.ul-mh').children().matchHeight();
	}
	
	/*_____________ progress bar _____________*/

	jQuery(".progress").loading();

	/*_____________ active widget _____________*/
	if(jQuery(".widget").length){
		jQuery(".widget__open").on('click', function(){		
			jQuery(".widget").addClass("open");
		});
		jQuery(".widget__close").on('click', function(){
			jQuery(".widget").removeClass("open");
		});
	}
	

	/*_____________ start exam _____________*/

	if (jQuery('.startExam').length) {
		jQuery('.startExam').on('click', function(){
			jQuery('.beforeStart').addClass('close');

		});



	}

	/*_____________ hobby _____________*/

	if (jQuery(".hobby_icon").length) {
		jQuery(".hobby_icon").each(function(){
			jQuery(".hobby_icon").on('click',function(e){
				e.preventDefault();
				jQuery(".hobby_icon").removeClass("active");
				jQuery(this).addClass("active");    

				var hobby_entry_active = jQuery(this).attr('href');
				jQuery('.hobby_entry').removeClass('active');
				jQuery(hobby_entry_active).addClass("active");

			});
		});
	}

	/*_____________ week progress _____________*/
	if (jQuery('.week_progress').length) {
		jQuery('.week_progress').each(function(){
			var val = jQuery(this).data('val');
			jQuery(this).css('bottom', val + '%');
		});
	}

	/*_____________ setInterva _____________*/


	// setInterval(function () {
	// 	var d = new Date();
	// 	var seconds = d.getMinutes() * 60 + d.getSeconds(); 
	// 	var mins_15 = 60 * 5; 
	// 	var timeleft = mins_15 - seconds % mins_15; 
	// 	var result = parseInt(timeleft / 60) + ':' + timeleft % 60; 
	// 	document.getElementById('countdown').innerHTML = result;

	// }, 1000);



/////////////
});
