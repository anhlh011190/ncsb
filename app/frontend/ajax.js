var models = require("./../models/load");
var Phonetics = models.Phonetics;
var Phonetics_config = models.Phonetics_config;
var View = models.View;
var Datetime = models.Datetime;
var ObjectId = require('mongodb').ObjectID;


module.exports = function(app, passport) {
  //Hien thi list các phonetics và thong tin cua phonetics đầu tiên
  app.post('/ajax.html' ,function(req, res) {
  	var d = new Date();
  	console.log(req.body)
  	switch (req.body.typeaction){
  		case "viewplus":
	  	  var view_info = {
    			type: req.body.type,
    			day: req.body.date,
    			userid: req.body.userid
	  	  }
  		  View.getlists(view_info,0,1).then(function(views){
  		  	if(views.length == 1){
  		  	  View.update(view_info,{total:parseInt(views[0].total + 1)}).then(function(){
  		  	  	res.write('1');
  		  	  	res.end();
  		  	  });
  		  	}else{
  		  	  var viewadd_info = {
  		  	  	type: req.body.type,
    	  			day: req.body.date,
    	  			userid: req.body.userid,
    	  			total: 1,
    	  			timecreated: Math.floor(d.getTime()/1000),
          		timemodified: Math.floor(d.getTime()/1000)
  		  	  }
  		  	  View.create(viewadd_info).then(function(){
  		  	  	res.write('1');
  		  	  	res.end();
  		  	  });
  		  	}
  		  });
  		  break;
  		default:
  		  res.write('01');
  		  res.end();
  		  break;
  	}
  });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}