var models = require("./../models/load");
var Grammar = models.Grammar;
var Day = models.Day_grammar_info;
var Grammar_config = models.Grammar_config;
var Datetime = models.Datetime;
var Day = models.Day;
var ObjectId = require('mongodb').ObjectID;

module.exports = function(app, passport) {

  app.get('/grammars.html',isLoggedIn, function(req, res) {
    Grammar.getlists({type_grammar:"TENSE"},0,1000).then(function(grammar){
       Grammar.getlists({type_grammar:"WORD USAGE"},0,1000).then(function(grammar1){
          Grammar.getlists({type_grammar:"SENTENCE STRUCTURE"},0,1000).then(function(grammar2){
            Grammar.getlists({type_grammar:"QUESTION"},0,1000).then(function(grammar3){
              Grammar.getlists({type_grammar:"MODAL VERB"},0,1000).then(function(grammar4){
                      if(grammar.length == 1 || 1){
                        var data = {
                          extractScripts: true,
                          title: "Ngữ pháp",
                          grammar: grammar,
                          grammar1: grammar1,
                          grammar2: grammar2,
                          grammar3: grammar3,
                          grammar4: grammar4,
                          userid: req.user.id,
                        };
                    res.render('frontend/grammars/list', data);
                  }else{
                    res.redirect('/')
                  }
            }); 
           });
          });
        }); 
      }); 
  });
  app.get('/grammar/getdata',function(req,res){
    var data_id = req.query.dataid ;
      Grammar.getlists({id:data_id},0,1).then(function(grammar){
        var result = ''
          for (var i = 0; i < grammar.length; i++) {
            result = result + grammar[i].content
          }
          res.write(result)
          res.end();
      });
  });
  app.get('/grammar.html',isLoggedIn,function(req, res) {
    if (typeof req.query.date !== 'undefined') {
      date = req.query.date
    }else{
      date = Datetime.getdate()
    }
    Grammar_config.getlists_2({"day":date},0,1).then(function(grammar){
      if(grammar.length == 1){
        Day.getlists({"day":date},0,1).then(function(day){
          if(day.length == 1){
            var data = {
              title: "Ngữ pháp",
              userid: req.user.id,
              grammar: grammar[0],
              sologan_grammar: day[0].title,
              day:date,
              control:"grammar"
            };
            res.render('frontend/grammars/one', data);
          }else{
            res.redirect('/')
          }
        }); 
      }else{
        res.redirect('/')
      }
    });  
  });
};

/**
 * [isLoggedIn Kiem tra dang nhap]
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {Boolean}       [description]
 */
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}



