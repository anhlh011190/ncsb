var models = require("./../models/load");
var Day = models.Day;
var Datetime = models.Datetime;


module.exports = function(app, passport) {
    app.get('/', isLoggedIn,function(req, res) {
        if (typeof req.query.date !== 'undefined') {
            date = req.query.date
        } else {
            date = Datetime.getdate()
        }
        var autostart = 0;
        if (typeof req.query.auto !== 'undefined') {
            autostart = 1;
        }
        Day.getlists({ "day": date }, 0, 1).then(function(day) {
            var data = {
                title: "Home page",
                layout: "frontend/giao_dien_moi/index",
                videowarmup: day[0],
                autostart: autostart,
                userid: req.user.id,
                date: date
            };
            res.render('', data);
        });
    });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}
