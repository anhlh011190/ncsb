var models = require("./../models/load");
var Phonetics = models.Phonetics;
var Phonetics_config = models.Phonetics_config;
var Day = models.Day;
var Datetime = models.Datetime;
var ObjectId = require('mongodb').ObjectID;


module.exports = function(app, passport) {
  //Hien thi list các phonetics và thong tin cua phonetics đầu tiên
  app.get('/phonetics.html', isLoggedIn ,function(req, res) {
    Phonetics.getlists({},0,1000).then(function(phonetics){
      if(phonetics.length == 1 || 1){
        var data = {
          title: "Ngữ âm",
          phonetics: phonetics,
          userid: req.user.id,
        };
        res.render('frontend/phonetics/list', data);
      }else{
        res.redirect('/')
      }
    });  
  });
  //Hien thi thong tin cua phonetics ngày hiện tại
  app.get('/phonetic.html', isLoggedIn ,function(req, res) {
    if (typeof req.query.date !== 'undefined') {
      date = req.query.date
    }else{
      date = Datetime.getdate()
    }
    var d = new Date();
    Phonetics_config.getlists_2({"day":date},0,1).then(function(phonetics){
      if(phonetics.length == 1){
        Day.getlists({"day":date},0,1).then(function(day){
          if(day.length == 1){
            var data = {
              title: "Ngữ âm",
              phonetics: phonetics[0],
              sologan: day[0].title,
              date:date,
              userid: req.user.id,
              control:"phonetic"
            };
            res.render('frontend/phonetics/one', data);
          }else{
            res.redirect('/')
          }
        });
      }else{
        res.redirect('/')
      }
    });  
  });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}


