var models = require("./../models/load");
var Phonetics = models.Phonetics;
var Typegrammar = models.Typegrammar;
var Grammar_config = models.Grammar_config;
var Phonetics_config = models.Phonetics_config;
var Vocabulary_config = models.Vocabulary_config;
var Vocabulary = models.Vocabulary;
var Exercise = models.Exercise;
var Result = models.Result;
var Resultspecial = models.Resultspecial;
var Day = models.Day;
var Datetime = models.Datetime;
var Vocabulary_week   = models.Vocabulary_week;
var View   = models.View;
var ObjectId = require('mongodb').ObjectID;


module.exports = function(app, passport) {
  app.get('/dev.html',function(req, res) {
    var day = Datetime.getdate();
    var data = {
      title: "Kiểm tra 1",
      userid: 'req.user.id',
      date: day
    };
    res.render('frontend/exercise/dev', data);
  });

  app.get('/exercise/grammar.html',isLoggedIn,function(req, res) {
    Typegrammar.getlists({},0,1000).then(function(grammars){
      Exercise.getlists({idparent:""+grammars[1].grammars[0]._id},0,1000).then(function(exercises){
      console.log(exercises)
        var data = {
          title: "Kiểm tra 1",
          grammars: grammars,
          userid: req.user.id,
          exercises: exercises
        };
        res.render('frontend/exercise/grammar', data);
      });
        
    });
  });

  app.get('/exercise/vocabulary.html',isLoggedIn,function(req, res) {
    var day = Datetime.getdate();
    Vocabulary_week.getlists({},0,14).then(function(weeks){
      Day.getlists_withday({day:day},0,1000).then(function(week_days){
        Day.getlists({idweek:""+week_days[0].vocabulary_week[0]._id},0,1000).then(function(days){
          Vocabulary_config.getlists({day:days[0].day},0,1).then(function(voca_config){
            if(voca_config.length != 0){
              var id_vocas = voca_config[0].list_id.substring(1,voca_config[0].list_id.length)
              Vocabulary.getlists_id(id_vocas.split(','),0,100).then(function(vocabularies){
                Exercise.getlists({idparent:""+vocabularies[0]._id},0,1000).then(function(exercises){
                  console.log(exercises)
                  var data = {
                    title: "Kiểm tra 1",
                    userid: 'req.user.id',
                    layout: 'frontend/exercise/vocabulary',
                    weeks: weeks,
                    week_current: week_days[0].vocabulary_week[0],
                    days: days,
                    vocabularies: vocabularies,
                    exercises: exercises
                  };
                  res.render('', data);
                });
              });  
            }else{
              res.write("Ngày "+days[0].day+" chưa config dữ liệu")
              res.end()
            }
          });
        });
      });
    });
  });  

  app.get('/profile.html',isLoggedIn ,function(req, res) {
    var date_begin = '2017-03-14'
    var user = req.user.id
    var date = Datetime.getdate()
    switch (date){
      case '2017-03-14':
      var total_day = 1
        break;
      case '2017-03-15':
      var total_day = 2
        break;
      case '2017-03-16':
      var total_day = 3
        break;
      case '2017-03-17':
      var total_day = 4
        break;
    }
    View.getlists({userid:user,type:"phonetic"},0,1000).then(function(phonetic_info){
      View.getlists({userid:user,type:"grammar"},0,1000).then(function(grammar_info){
        View.getlists({userid:user,type:"vocabulary"},0,1000).then(function(vocabulary_info){
          Resultspecial.getlists2({userid:user},0,1000).then(function(result_info){
            Day.getlists({"day":date},0,1).then(function(day){
              var point_phonetic_cl = 0
              var point_grammar_cl = 0
              var point_vocabulary_cl = 0
              var point_phonetic_sl = 0
              var point_grammar_sl = 0
              var point_vocabulary_sl = 0
              for (var i = 0; i < phonetic_info.length; i++) {
                if(date_begin <= phonetic_info[i].day &&  phonetic_info[i].day <= date){
                  point_phonetic_sl += 1
                }
              }
              for (var i = 0; i < grammar_info.length; i++) {
                if(date_begin <= grammar_info[i].day &&  grammar_info[i].day <= date){
                  point_grammar_sl += 1
                }
              }
              for (var i = 0; i < vocabulary_info.length; i++) {
                if(date_begin <= vocabulary_info[i].day &&  vocabulary_info[i].day <= date){
                  point_vocabulary_sl += 1
                }
              }
              for (var i = 0; i < result_info.length; i++) {
                point_phonetic_cl += result_info[i].phonetic
                point_grammar_cl += result_info[i].grammar
                point_vocabulary_cl += result_info[i].vocabulary
              }
              var data = {
                title: "Profile",
                date: date,
                control:"exercise",
                sologan:day[0].title,
                userid: user,
                point_phonetic_sl: point_phonetic_sl,
                point_grammar_sl: point_grammar_sl,
                point_vocabulary_sl: point_vocabulary_sl,
                point_phonetic_cl: point_phonetic_cl,
                point_grammar_cl: point_grammar_cl,
                point_vocabulary_cl: point_vocabulary_cl,
                total_cl: result_info.length,
                total_day: total_day
              }
              console.log(total_day)
              console.log(data)
              res.render('frontend/exercise/profile', data);
            });
          });
        });
      });
    });
  });

  app.get('/exercises.html',isLoggedIn,function(req, res) {
    var day = Datetime.getdate();
    Vocabulary_week.getlists({},0,14).then(function(weeks){
      Day.getlists_withday({day:day},0,1000).then(function(week_days){
        Day.getlists({idweek:""+week_days[0].vocabulary_week[0]._id},0,1000).then(function(days){
          var data = {
            title: "Kiểm tra 1",
            userid: req.user.id,
            layout: 'frontend/exercise/all',
            weeks: weeks,
            week_current: week_days[0].vocabulary_week[0],
            days:days
          };
          res.render('', data);
        });
      });
    });
  });

  app.get('/exercise/phonetics.html',function(req, res) {
    Phonetics.getlists({},0,1000).then(function(phonetics){
      if(phonetics.length != 0){
        Exercise.getlists({idparent:phonetics[0].id},0,1000).then(function(exercises){
          console.log(exercises[0])
          var data = {
            title: "Ngữ âm",
            phonetics: phonetics,
            userid: req.user.id,
            exercises: exercises
          };
          res.render('frontend/exercise/phonetics', data);
        });
      }else{
        res.redirect('/')
      }
    });
  });

  app.get('/exercise/week.html',isLoggedIn,function(req, res) {
    var d = new Date();
    var day = Datetime.getdate();
    Vocabulary_week.getlists({week:req.query.week},0,14).then(function(weeks){
      if(weeks.length == 1){
          Day.getlists({idweek:""+weeks[0]._id},0,1000).then(function(days){
            if(days.length != 0){
              Result.getlists(days[0].day,req.user.id,0,0,0,1).then(function(result){
                var date = days[0].day
                if(result.length == 1){
                  Exercise.getlists_2(result[0].id_exercise.split(","),0,15).then(function(exercises){
                    var data = {
                      title: "Kiểm tra 2",
                      userid: req.user.id,
                      layout: 'frontend/exercise/week',
                      weeks: weeks,
                      days: days,
                      date: date,
                      exercises: exercises
                    };
                    res.render('', data);
                  });
                }else{
                  Phonetics_config.getlists({"day":date},0,1).then(function(phonetics){
                    if(phonetics.length == 1){
                      Exercise.getlists({"type":"phonetics","idparent":""+phonetics[0].idphonetic},0,5).then(function(exercise_phonetics){
                        Grammar_config.getlists_2({"day":date},0,1).then(function(grammar){
                          if(grammar.length == 1){
                            Exercise.getlists({"type":"grammar","idparent":""+grammar[0].idgrammar},0,5).then(function(exercise_grammars){
                              Vocabulary_config.getlists({"day":date},0,1).then(function(vocabulary){
                                if(vocabulary.length == 1){
                                  Exercise.getlists_vocabularies(vocabulary[0].list_id.substring(1,vocabulary[0].list_id.length-1).split(","),0,5).then(function(exercise_vocabularies){
                                    //phần hiển thị kết quả
                                    var content_result = ''
                                    var id_exercise = ''
                                    //lấy kết quả và nội dung câu hỏi phẩn phonetics
                                    for (var i = 0; i < exercise_phonetics.length; i++) {
                                      //Viết dạng json này để tiện cho việc cắt chuỗi ứng với mỗi câu hỏi sẽ có một mảng json đi cùng bao gồm id bài tập kết quả sự lựa chọn của người dùng
                                      //id_exercie này được dùng cho việc lấy lại bài tập nếu người dùng thoát ra vào lại nhưng chưa hoàn thành hết số câu hỏi cho ra
                                      var content = JSON.parse(exercise_phonetics[i].content);
                                      content_result = content_result + '{"idexercise":"'+exercise_phonetics[i]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                                      id_exercise = id_exercise + exercise_phonetics[i]._id + ","
                                    }
                                    //lấy kết quả phần grammars
                                    for (var j = 0; j < exercise_grammars.length; j++) {
                                      //Tương tự với phần grammar
                                      var content = JSON.parse(exercise_grammars[j].content);
                                      content_result = content_result + '{"idexercise":"'+exercise_grammars[j]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                                      id_exercise = id_exercise + exercise_grammars[j]._id + ","
                                    }
                                    //lấy kết quả phân vocabularies
                                    for (var k = 0; k < exercise_vocabularies.length; k++) {
                                      var content = JSON.parse(exercise_vocabularies[k].content);
                                      content_result = content_result + '{"idexercise":"'+exercise_vocabularies[k]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                                      id_exercise = id_exercise + exercise_vocabularies[k]._id + ","
                                    }
                                    // lưu vào biến kết quả bào gồm userid content i
                                    //  biến content được xử lý chuỗi bằng việc lấy từ ký tự 0 và đến trước 3 ký tự cuối cùng là ___
                                    //  tương tự với id_exercise để loại bỏ ký ,
                                    //  điểm mặc định lúc này là 0 thời gian tạo trạng thái làm bài
                                    var result_content = {
                                      userid: req.user.id,
                                      content : content_result.substring(0,content_result.length-3),
                                      id_exercise: id_exercise.substring(0,id_exercise.length-1),
                                      point:0,
                                      timecreated: Math.floor(d.getTime()/1000),
                                      status:0,
                                      vocabulary:0,
                                      phonetic:0,
                                      grammar:0,
                                      day:date,
                                    }
                                    var result_special_content = {
                                      userid: req.user.id,
                                      point:0,
                                      timecreated: Math.floor(d.getTime()/1000),
                                      status:0,
                                      day:date,
                                      vocabulary:0,
                                      phonetic:0,
                                      grammar:0
                                    }
                                    //Tạo kết quả
                                    Result.create(result_content).then(function(){
                                      /*------------------------------Start-----------------------------------*/
                                      //tao them bang dac biet nay de biet trong ngay ho lam den cai gi
                                      Resultspecial.count({userid: req.user.id,day:date}).then(function(total_result){
                                          if(total_result == 1){
                                            Exercise.getlists_2(id_exercise.substring(0,id_exercise.length-1).split(","),0,15).then(function(exercises){
                                                Day.getlists({"day":date},0,1).then(function(day){
                                                  if(day.length == 1){
                                                    var data = {
                                                      title: "Kiểm tra 2",
                                                      userid: req.user.id,
                                                      layout: 'frontend/exercise/week',
                                                      weeks: weeks,
                                                      days: days,
                                                      date: date,
                                                      exercises: exercises
                                                    };
                                                    res.render('', data);
                                                  }else{
                                                    res.redirect('/')
                                                  }
                                                });
                                              });
                                        }else{
                                          Resultspecial.create(result_special_content).then(function(){
                                              Exercise.getlists_2(id_exercise.substring(0,id_exercise.length-1).split(","),0,15).then(function(exercises){
                                                Day.getlists({"day":date},0,1).then(function(day){
                                                  if(day.length == 1){
                                                    var data = {
                                                      title: "Kiểm tra 2",
                                                      userid: req.user.id,
                                                      layout: 'frontend/exercise/week',
                                                      weeks: weeks,
                                                      days: days,
                                                      date: date,
                                                      exercises: exercises
                                                    };
                                                    res.render('', data);
                                                  }else{
                                                    res.redirect('/')
                                                  }
                                                });
                                              });
                                          });
                                        }
                                      });
                                      /*------------------------------End-----------------------------------*/    
                                    });
                                  });
                                }else{
                                  res.redirect('/')
                                }
                              });
                            });
                          }else{
                            res.redirect('/')
                          }
                        });  
                      });
                    }else{
                      res.redirect('/')
                    }
                  }); 
                }
              });
            }else{
              res.redirect('/');
            }
            
          });
      }else{
        res.redirect('/');
      }
        
    });
  });

  //Hiển thị bài tập theo ngày 
  app.get('/exercise.html' ,isLoggedIn,function(req, res) {
    var d = new Date();
    if (typeof req.query.date !== 'undefined') {
      date = req.query.date
    }else{
      date = Datetime.getdate()
    }
    //Lấy list bài tập của học viên ??? parseInt(Math.floor(d.getTime()/1000) -900) không có ý nghĩa
    //function bên model cho Result.getlists function(userid,timecreated,status,start,display)
    // status truyền vào = 0 tức là lấy kết quả khi học viên đang học chưa ấn nộp bài rất quan trọng cái status này 
    // 
    Result.getlists(date,req.user.id,parseInt(Math.floor(d.getTime()/1000) -900),0,0,1).then(function(result){
      //Nếu số lượng kết quả của học viên đúng = 1
      //Lấy list bài tập excerse cho học viên số lượng giới hạn 15
      //Lấy title của ngày hiện tại hiện cho phần exercise
      if(result.length == 1){
        Exercise.getlists_2(result[0].id_exercise.split(","),0,15).then(function(exercises){
          Day.getlists({"day":date},0,1).then(function(day){
            if(day.length == 1){
              var data = {
                title: "Kiểm tra",
                exercises: exercises,
                countdown : 900,
                userid: req.user.id,
                sologan:day[0].title,
                date_next:Datetime.getdate_next(date),
                date:date,
                control:"exercise"
              };
              res.render('frontend/exercise/day', data);
            }else{
              res.redirect('/')
            }
          });
        });
      }
      //Còn nếu result.length == 0 tức là học viên này chưa làm bài dựa thoe req.user.id
      //Lấy bài tập mới nhất cho học viên
      else{
        //Lấy bài phát âm của ngày hôm đó SL = 1
        //Láy phần bài tập tương ứng với phần idparent = idphonetic số lượng 5 câu
        //Tương tự với phần grammar
        //Đến phần vocabulary nhận được một chuỗi các id của voca xư lý chuỗi vocabulary[0].list_id.substring(1,vocabulary[0].list_id.length-1).split(","),0,5
        //Chưa random lấy 5 câu hỏi tương ứng với từ vựng vẫn lấy câu hỏi gân/xa nhất tương ứng với các từ vựng trong list 
        //Ví dụ có 6 từ abcdef học nhưng a có 10câu liên quan b có 20 câu về sau lấy mõi 5 câu của thằng a ??? 
        Phonetics_config.getlists({"day":date},0,1).then(function(phonetics){
          if(phonetics.length == 1){
            Exercise.getlists({"type":"phonetics","idparent":""+phonetics[0].idphonetic},0,5).then(function(exercise_phonetics){
              Grammar_config.getlists_2({"day":date},0,1).then(function(grammar){
                if(grammar.length == 1){
                  Exercise.getlists({"type":"grammar","idparent":""+grammar[0].idgrammar},0,5).then(function(exercise_grammars){
                    Vocabulary_config.getlists({"day":date},0,1).then(function(vocabulary){
                      if(vocabulary.length == 1){
                        Exercise.getlists_vocabularies(vocabulary[0].list_id.substring(1,vocabulary[0].list_id.length-1).split(","),0,5).then(function(exercise_vocabularies){
                          //phần hiển thị kết quả
                          var content_result = ''
                          var id_exercise = ''
                          //lấy kết quả và nội dung câu hỏi phẩn phonetics
                          for (var i = 0; i < exercise_phonetics.length; i++) {
                          	//Viết dạng json này để tiện cho việc cắt chuỗi ứng với mỗi câu hỏi sẽ có một mảng json đi cùng bao gồm id bài tập kết quả sự lựa chọn của người dùng
                          	//id_exercie này được dùng cho việc lấy lại bài tập nếu người dùng thoát ra vào lại nhưng chưa hoàn thành hết số câu hỏi cho ra
                            var content = JSON.parse(exercise_phonetics[i].content);
                            content_result = content_result + '{"idexercise":"'+exercise_phonetics[i]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                            id_exercise = id_exercise + exercise_phonetics[i]._id + ","
                          }
                          //lấy kết quả phần grammars
                          for (var j = 0; j < exercise_grammars.length; j++) {
                            //Tương tự với phần grammar
                            var content = JSON.parse(exercise_grammars[j].content);
                            content_result = content_result + '{"idexercise":"'+exercise_grammars[j]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                            id_exercise = id_exercise + exercise_grammars[j]._id + ","
                          }
                          //lấy kết quả phân vocabularies
                          for (var k = 0; k < exercise_vocabularies.length; k++) {
                            var content = JSON.parse(exercise_vocabularies[k].content);
                            content_result = content_result + '{"idexercise":"'+exercise_vocabularies[k]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                            id_exercise = id_exercise + exercise_vocabularies[k]._id + ","
                          }
                          // lưu vào biến kết quả bào gồm userid content i
                          //  biến content được xử lý chuỗi bằng việc lấy từ ký tự 0 và đến trước 3 ký tự cuối cùng là ___
                          //  tương tự với id_exercise để loại bỏ ký ,
                          //  điểm mặc định lúc này là 0 thời gian tạo trạng thái làm bài
                          var result_content = {
                            userid: req.user.id,
                            content : content_result.substring(0,content_result.length-3),
                            id_exercise: id_exercise.substring(0,id_exercise.length-1),
                            point:0,
                            timecreated: Math.floor(d.getTime()/1000),
                            status:0,
                            vocabulary:0,
                            phonetic:0,
                            grammar:0,
                            day:date,
                          }
                          var result_special_content = {
                            userid: req.user.id,
                            point:0,
                            timecreated: Math.floor(d.getTime()/1000),
                            status:0,
                            day:date,
                            vocabulary:0,
                            phonetic:0,
                            grammar:0
                          }
                          //Tạo kết quả
                          Result.create(result_content).then(function(){
                            /*------------------------------Start-----------------------------------*/
                            //tao them bang dac biet nay de biet trong ngay ho lam den cai gi
                            Resultspecial.count({userid: req.user.id,day:date}).then(function(total_result){
                              	if(total_result == 1){
                                 	Exercise.getlists_2(id_exercise.substring(0,id_exercise.length-1).split(","),0,15).then(function(exercises){
	                                    Day.getlists({"day":date},0,1).then(function(day){
	                                      if(day.length == 1){
	                                        var data = {
	                                          title: "Kiểm tra",
	                                          exercises: exercises,
	                                          countdown : 900,
	                                          userid: req.user.id,
	                                          sologan:day[0].title,
	                                          date_next:Datetime.getdate_next(date),
                                            date:date,
                                            control:"exercise"
	                                        };
	                                        res.render('frontend/exercise/day', data);
	                                      }else{
	                                        res.redirect('/')
	                                      }
	                                    });
                                  	});
                              }else{
                                Resultspecial.create(result_special_content).then(function(){
                                  	Exercise.getlists_2(id_exercise.substring(0,id_exercise.length-1).split(","),0,15).then(function(exercises){
	                                    Day.getlists({"day":date},0,1).then(function(day){
	                                      if(day.length == 1){
	                                        var data = {
	                                          title: "Kiểm tra",
	                                          exercises: exercises,
	                                          countdown : 900,
	                                          userid: req.user.id,
	                                          sologan:day[0].title,
                                            date_next:Datetime.getdate_next(date),
                                            date:date,
                                            control:"exercise"
	                                        };
	                                        res.render('frontend/exercise/day', data);
	                                      }else{
	                                        res.redirect('/')
	                                      }
	                                    });
                                  	});
                                });
                              }
                            });
                            /*------------------------------End-----------------------------------*/    
                          });
                        });
                      }else{
                        res.redirect('/')
                      }
                    });
                  });
                }else{
                  res.redirect('/')
                }
              });  
            });
          }else{
            res.redirect('/')
          }
        });  
      }
    });    
  });
  
  app.post('/exercise/action' ,function(req, res) {
    switch (req.body.type){
      case "week":
        Day.getlists({idweek:req.body.idweek},0,1000).then(function(days){
          var result = JSON.stringify(days)
          res.write(result)
          res.end();
        });
        break;
      case "voca_week":
        Vocabulary_config.getlists({day:req.body.day},0,1).then(function(voca_config){
          if(voca_config.length != 0){
            var id_vocas = voca_config[0].list_id.substring(1,voca_config[0].list_id.length)
            Vocabulary.getlists_id(id_vocas.split(','),0,100).then(function(vocabularies){
              var result = JSON.stringify(vocabularies)
              res.write(result)
              res.end();
            });  
          }else{
            res.write("Ngày " + req.body.day + " chưa config dữ liệu")
            res.end()
          }
        });
        break;
      case "grammar":
        Exercise.getlists({idparent:req.body.idgrammar},0,1000).then(function(exercises){
          var result = JSON.stringify(exercises)
          res.write(result)
          res.end();
        });
        break;
      case "phonetic":
        Exercise.getlists({idparent:req.body.idphonetic},0,1000).then(function(exercises){
          var result = JSON.stringify(exercises)
          res.write(result)
          res.end();
        });
        break;
      case "vocabulary":
        Exercise.getlists({idparent:req.body.idvocabulary},0,1000).then(function(exercises){
          var result = JSON.stringify(exercises)
          res.write(result)
          res.end();
        });
        break;
      case "check":
        Exercise.getlists({_id:ObjectId(req.body.name)},0,1000).then(function(exercises){
          console.log(exercises)
          var result = ''
          var content = JSON.parse(exercises[0].content);
          if(content.answer == req.body.val){
            result += "<div class='correct'><p style='color:#0db10c;font-size:18px;'><i class='fa fa-check'></i> Đúng</p>"
          }else{
            result += "<div class='incorrect'><p style='color:red;font-size:18px;'><i class='fa fa-times'></i> Sai</p>"
          }
          res.write(result+content.giaithich+"</div>")
          res.end();
        });
        break;
      case "days":
      //Lấy dữ liệu các chủ điểm của 1 tuần
        Day.getlists({idweek:req.body.week_id},0,1000).then(function(days){
          var result = ''
          if(days.length != 0){
            for (var i = 0; i < days.length; i++) {
              result = result + '<li><a href="exercise.html?date='+days[i].day+'">'+days[i].title+'</a></li>'
            }
            res.write(result)
            res.end();
          }else{
            res.write("Chưa có dữ liệu")
            res.end();
          } 
            
        });
        break;
      case "day_one":
          var d = new Date();
          var date = req.body.day;
          Result.getlists(date,req.user.id,0,0,0,1).then(function(result){
            //Nếu số lượng kết quả của học viên đúng = 1
            //Lấy list bài tập excerse cho học viên số lượng giới hạn 15
            //Lấy title của ngày hiện tại hiện cho phần exercise
            if(result.length == 1){
              Exercise.getlists_2(result[0].id_exercise.split(","),0,15).then(function(exercises){
                Day.getlists({"day":date},0,1).then(function(day){
                  if(day.length == 1){
                    var result = JSON.stringify(exercises)
                    res.write(result)
                    res.end();
                  }else{
                    var result = JSON.stringify(exercises)
                    res.write(result)
                    res.end();
                  }
                });
              });
            }
            //Còn nếu result.length == 0 tức là học viên này chưa làm bài dựa thoe req.user.id
            //Lấy bài tập mới nhất cho học viên
            else{
              //Lấy bài phát âm của ngày hôm đó SL = 1
              //Láy phần bài tập tương ứng với phần idparent = idphonetic số lượng 5 câu
              //Tương tự với phần grammar
              //Đến phần vocabulary nhận được một chuỗi các id của voca xư lý chuỗi vocabulary[0].list_id.substring(1,vocabulary[0].list_id.length-1).split(","),0,5
              //Chưa random lấy 5 câu hỏi tương ứng với từ vựng vẫn lấy câu hỏi gân/xa nhất tương ứng với các từ vựng trong list 
              //Ví dụ có 6 từ abcdef học nhưng a có 10câu liên quan b có 20 câu về sau lấy mõi 5 câu của thằng a ??? 
              Phonetics_config.getlists({"day":date},0,1).then(function(phonetics){
                if(phonetics.length == 1){
                  Exercise.getlists({"type":"phonetics","idparent":""+phonetics[0].idphonetic},0,5).then(function(exercise_phonetics){
                    Grammar_config.getlists_2({"day":date},0,1).then(function(grammar){
                      if(grammar.length == 1){
                        Exercise.getlists({"type":"grammar","idparent":""+grammar[0].idgrammar},0,5).then(function(exercise_grammars){
                          Vocabulary_config.getlists({"day":date},0,1).then(function(vocabulary){
                            if(vocabulary.length == 1){
                              Exercise.getlists_vocabularies(vocabulary[0].list_id.substring(1,vocabulary[0].list_id.length-1).split(","),0,5).then(function(exercise_vocabularies){
                                //phần hiển thị kết quả
                                var content_result = ''
                                var id_exercise = ''
                                //lấy kết quả và nội dung câu hỏi phẩn phonetics
                                for (var i = 0; i < exercise_phonetics.length; i++) {
                                  //Viết dạng json này để tiện cho việc cắt chuỗi ứng với mỗi câu hỏi sẽ có một mảng json đi cùng bao gồm id bài tập kết quả sự lựa chọn của người dùng
                                  //id_exercie này được dùng cho việc lấy lại bài tập nếu người dùng thoát ra vào lại nhưng chưa hoàn thành hết số câu hỏi cho ra
                                  var content = JSON.parse(exercise_phonetics[i].content);
                                  content_result = content_result + '{"idexercise":"'+exercise_phonetics[i]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                                  id_exercise = id_exercise + exercise_phonetics[i]._id + ","
                                }
                                //lấy kết quả phần grammars
                                for (var j = 0; j < exercise_grammars.length; j++) {
                                  //Tương tự với phần grammar
                                  var content = JSON.parse(exercise_grammars[j].content);
                                  content_result = content_result + '{"idexercise":"'+exercise_grammars[j]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                                  id_exercise = id_exercise + exercise_grammars[j]._id + ","
                                }
                                //lấy kết quả phân vocabularies
                                for (var k = 0; k < exercise_vocabularies.length; k++) {
                                  var content = JSON.parse(exercise_vocabularies[k].content);
                                  content_result = content_result + '{"idexercise":"'+exercise_vocabularies[k]._id+'","answer":"'+content.answer+'","user_select":"0"}___'
                                  id_exercise = id_exercise + exercise_vocabularies[k]._id + ","
                                }
                                // lưu vào biến kết quả bào gồm userid content i
                                //  biến content được xử lý chuỗi bằng việc lấy từ ký tự 0 và đến trước 3 ký tự cuối cùng là ___
                                //  tương tự với id_exercise để loại bỏ ký ,
                                //  điểm mặc định lúc này là 0 thời gian tạo trạng thái làm bài
                                var result_content = {
                                  userid: req.user.id,
                                  content : content_result.substring(0,content_result.length-3),
                                  id_exercise: id_exercise.substring(0,id_exercise.length-1),
                                  point:0,
                                  timecreated: Math.floor(d.getTime()/1000),
                                  status:0,
                                  vocabulary:0,
                                  phonetic:0,
                                  grammar:0,
                                  day:date,
                                }
                                var result_special_content = {
                                  userid: req.user.id,
                                  point:0,
                                  timecreated: Math.floor(d.getTime()/1000),
                                  status:0,
                                  day:date,
                                  vocabulary:0,
                                  phonetic:0,
                                  grammar:0
                                }
                                //Tạo kết quả
                                Result.create(result_content).then(function(){
                                  /*------------------------------Start-----------------------------------*/
                                  //tao them bang dac biet nay de biet trong ngay ho lam den cai gi
                                  Resultspecial.count({userid: req.user.id,day:date}).then(function(total_result){
                                      if(total_result == 1){
                                        Exercise.getlists_2(id_exercise.substring(0,id_exercise.length-1).split(","),0,15).then(function(exercises){
                                            Day.getlists({"day":date},0,1).then(function(day){
                                              if(day.length == 1){
                                                var result = JSON.stringify(exercises)
                                                res.write(result)
                                                res.end();
                                              }else{
                                                var result = JSON.stringify(exercises)
                                                res.write(result)
                                                res.end();
                                              }
                                            });
                                          });
                                    }else{
                                      Resultspecial.create(result_special_content).then(function(){
                                          Exercise.getlists_2(id_exercise.substring(0,id_exercise.length-1).split(","),0,15).then(function(exercises){
                                            Day.getlists({"day":date},0,1).then(function(day){
                                              if(day.length == 1){
                                                var result = JSON.stringify(exercises)
                                                res.write(result)
                                                res.end();
                                              }else{
                                                var result = JSON.stringify(exercises)
                                                res.write(result)
                                                res.end();
                                              }
                                            });
                                          });
                                      });
                                    }
                                  });
                                  /*------------------------------End-----------------------------------*/    
                                });
                              });
                            }else{
                              res.write("ANHLH2 3")
                              res.end();
                            }
                          });
                        });
                      }else{
                        res.write("ANHLH2 2")
                        res.end();
                      }
                    });  
                  });
                }else{
                  res.write("ANHLH2 1")
                  res.end();
                }
              });  
            }
          }); 
        break;
      default :
        break;
    }
  });

  app.post('/exercise.html' ,function(req, res) {
    Result.getlists(req.body.date,req.user.id,0,0,0,1).then(function(result){
      var content = result[0].content.replace(/\r?\n|\r/g,'').split("___")
      var flag = 0;
      var content_update = ""
      for (var i = 0; i < content.length; i++) {
        var abc = content[i].replace(/\r?\n|\r/g,'')
        var content_one = JSON.parse(abc);
        if(content_one.idexercise == req.body.name){
          if(content_one.answer == req.body.val && content_one.answer != content_one.user_select){
            flag = 1;
          }
          if(content_one.answer != req.body.val && content_one.answer == content_one.user_select){
            flag = -1;
          }
          console.log(content_one.answer)
          content_one.user_select = req.body.val
          if(content_one.answer == req.body.val){
            var finish = "<div class='correct'><p style='color:#0db10c;font-size:18px;'><i class='fa fa-check'></i> Đúng</p>"
          }else{
            var finish = "<div class='incorrect'><p style='color:red;font-size:18px;'><i class='fa fa-times'></i> Sai</p>"
          }
        }
        content_update = content_update + JSON.stringify(content_one) +"___"
      }
      point = parseInt(result[0].point + flag)
      Exercise.getlists({"_id":ObjectId(req.body.name)},0,1).then(function(exercises){ 
        switch (exercises[0].type){
          case "phonetics":
            var phonetic = parseInt(result[0].phonetic + flag)
            Result.update({"_id":ObjectId(result[0]._id)},{phonetic:phonetic,point:point,"content":content_update.substring(0,content_update.length-3)},{}).then(function(){
              var content = JSON.parse(exercises[0].content);
              res.write(""+finish+content.giaithich+"</div>");
              res.end();
            });
            break;
          case "grammar":
            var grammar = parseInt(result[0].grammar + flag)
            Result.update({"_id":ObjectId(result[0]._id)},{grammar:grammar,point:point,"content":content_update.substring(0,content_update.length-3)},{}).then(function(){
              var content = JSON.parse(exercises[0].content);
              res.write(""+finish+content.giaithich+"</div>");
              res.end();
            });
            break;
          default:
            var vocabulary = parseInt(result[0].vocabulary + flag)
            Result.update({"_id":ObjectId(result[0]._id)},{vocabulary:vocabulary,point:point,"content":content_update.substring(0,content_update.length-3)},{}).then(function(){
              var content = JSON.parse(exercises[0].content);
              res.write(""+finish+content.giaithich+"</div>");
              res.end();
            });
            break;
        }
            
      });
    })
  });

  app.post('/exercise' ,function(req, res) {
    Result.getlists(req.body.date,req.user.id,0,0,0,1).then(function(result){
      Result.update({"_id":ObjectId(result[0]._id)},{status:1},{}).then(function(){
        Resultspecial.update({userid: req.user.id,day:req.body.date},{vocabulary:result[0].vocabulary,grammar:result[0].grammar,phonetic:result[0].phonetic,point:parseInt(result[0].point)},{}).then(function(){
          res.write(""+result[0].point);
          res.end();
        }); 
      });
    });
  });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}


