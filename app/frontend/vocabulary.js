////////////
//require //
////////////
var models            = require("./../models/load");
var Vocabulary_config = models.Vocabulary_config;
var ObjectId          = require('mongodb').ObjectID;
var Vocabulary        = models.Vocabulary;
var Vocabulary_week   = models.Vocabulary_week;
var Day = models.Day;
/**
 * Get thong tin can thiet
 * @return {[type]} [description]
 */
function get_date_now(){
    var d = new Date();
    var year = d.getFullYear();
    var month = parseInt(d.getMonth()+1);
    if(month < 10){
      month = "0"+month
    }
    var day = d.getDate();
    if(day < 10){
      day = "0"+day
    }
    var date = year+'-'+month+"-"+day;

    return date;
}
/**
 * bug
 */
function bug(a){
  console.log(a);
  return false;
}
//
function get_audio_or_image(){
  
}
//////////
//ROUTE //
//////////
module.exports = function(app, passport) {
  /*
  View vocabulary
   */
  app.get('/vocabulary.html',function(req, res) {
    if(typeof req.query.date === "undefined"){
      var ngay = get_date_now()
    }else{
      var ngay =  req.query.date
    }
    Vocabulary_config.getlists({"day":ngay},0,1000).then(function(vocabulary){
      if(vocabulary.length != 0){
        Vocabulary.getlists_id(vocabulary[0].list_id.replace(/"/g, '').split(",")).then(function(listvocabulary){
          console.log(listvocabulary)
          Day.getlists({"day":ngay},0,1).then(function(day){
            if(day.length == 1){
                var data = {
                    title:'VOCABULARY',
                    listvocabulary :listvocabulary,
                    sologan:day[0].title,
                    control:"vocabulary",
                    userid: 'req.user.id',
                    day:ngay,
                    date: ngay
                 }
                res.render('frontend/vocabulary/vocabulary', data);
            }else{
              res.redirect('/')
            }
          });
        });
      }else{
        res.redirect('/')
      }
       
    });
  });
/////////////////////////////////
//lấy chủ để tuần cho học viên //
/////////////////////////////////
  app.get('/vocabulary/week.html',isLoggedIn,function(req, res) {
  	var ngay = get_date_now()
  	//get list chủ để tuần và các ngày theo tuần
    Day.getlists_withday({},0,1000).then(function(vocabulary){
    	Vocabulary_week.getlists({},0,1000).then(function(week){
    	   	Day.getlists_withday({day:ngay},0,1000).then(function(voca){
		        var data = {
		       	  	layout: 'frontend/vocabulary/vocabulary-1',
		            title:'Học từ vựng',
		            vocabulary :vocabulary,
		            week : week,
                userid: req.user.id,
		            voca : voca[0].vocabulary_week
		        };
	       res.render('', data);
   			});
    	});   
    });
  });

  app.get('/vocabulary/week/content.html',isLoggedIn,function(req, res) {
    var ngay = get_date_now()
    //get list chủ để tuần và các ngày theo tuần
    Vocabulary_week.getlists({id:req.query.week},0,1000).then(function(week){
      Day.getlists({idweek:req.query.week},0,1000).then(function(vocabulary){
        var tmp = [];
        var tmp_day = [];
        for (var i = 0; i < vocabulary.length; i++) {
          tmp.push(vocabulary[i].day);
          tmp_day.push(vocabulary[i].id)
        };
        // console.log(tmp_day);return false;
        Vocabulary_config.getlists_date(tmp).then(function(vocalist){
          console.log(tmp_day[0]);
          var data = {
            layout: 'frontend/vocabulary/vocabulary-2',
            title:'Học từ vựng',
            vocalist:vocalist,
            listidday:tmp_day,
            userid: req.user.id,
            week:week,
            weekday:req.query.weekday
          };
          res.render('',data)
        });
      });
    });
  });

  app.get('/vocabulary/getdata',isLoggedIn,function(req,res){
    var data_id = req.query.dataid ;
      Vocabulary.getlists({id:data_id},0,1).then(function(voca){
        res.json(voca);
      });
  });

};
/**
 * [isLoggedIn Kiem tra dang nhap]
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {Boolean}       [description]
 */
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}
