var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var resultspecialChema = new Schema({
	userid: String,
	day:String,
	point:Number,
	timecreated: Number,
	status:Number,
	vocabulary:Number,
	phonetic:Number,
	grammar:Number
});
var Resultspecial = mongoose.model('Resultspecial',resultspecialChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var result = new Resultspecial(data);
			result.save(function(err,phonetics_data) {
				if (err) reject(err);
				console.log('Result saved successfully');
				resolve(phonetics_data);
			});
		});
	},
	getlists : function(userid,timecreated,status,start,display){
		return new Promise(function(resolve,reject){
			Resultspecial.aggregate([
				{ $match : { 
					// timecreated : { $gt : timecreated} ,
					userid : userid,
					status: status
					} 
				},
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	getlists2 : function(where,start,display){
		return new Promise(function(resolve,reject){
			Resultspecial.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Resultspecial.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Resultspecial.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,phonetics,type){
		return new Promise(function(resolve,reject){
			Resultspecial.update(where,phonetics,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}