var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var viewChema = new Schema({
	total : Number,
	userid: String,
	type: String,
	day: String,
	timecreated: String,
	timemodified: String
});
var View = mongoose.model('View',viewChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var grammar = new View(data);
			grammar.save(function(err,grammar_data) {
				if (err) reject(err);
				console.log('Grammar saved successfully');
				resolve(grammar_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			View.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			View.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			View.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,grammar,type){
		return new Promise(function(resolve,reject){
			View.update(where,grammar,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}