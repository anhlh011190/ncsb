/*Khởi tạo vocabulary*/
var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var vocabularysChema = new Schema({
	id: String,
	keyword: String,
	phonetic: String,
	content: String,
	audio: String,
	image: String,
	timecreated: String,
	timemodified: String,
	usercreated: String
});
var Vocabulary = mongoose.model('Vocabulary',vocabularysChema);
mongoose.Promise = global.Promise;

//RUN
module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var vocabulary = new Vocabulary(data);
			vocabulary.save(function(err,vocabulary_data) {
				if (err) reject(err);
				resolve(vocabulary_data);
				console.log('Vocabulary saved successfully');
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Vocabulary.aggregate([
				{ $match : where },
				{ $sort : 
			     { 
			      timecreated : 1
			     } 
			    },
				{ $limit : display },
				{ $skip : start }
           	],function(err, data){
           		if(err) reject(err);
				resolve(data);
           	});
		});
	},
	//Lay thong tin theo danh sach id
	getlists_id :function(where){
		return new Promise(function(resolve,reject){
			Vocabulary.aggregate([
				{ $match : { id : { $in :where} } }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Vocabulary.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Vocabulary.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,vocabulary,type){
		return new Promise(function(resolve,reject){
			Vocabulary.update(where,vocabulary,type,function(err, data) {
				if(err) reject(err);
				resolve(data);
			});
		});
	},
	//getlist_voca
	getlist_voca : function (data) {
		return new Promise(function(resolve,reject){
			Vocabulary.find({'keyword' : new RegExp(data, 'i')},function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}