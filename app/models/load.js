module.exports = {
	Phonetics : require('./phonetics.js'),
	Phonetics_config : require('./phonetics_config.js'),
	Grammar : require('./grammar.js'),
	Typegrammar : require('./typegrammar.js'),
	Grammar_config : require('./grammar_config.js'),
	Vocabulary : require('./vocabulary.js'),
	Vocabulary_config : require('./vocabulary_config.js'),
	Vocabulary_day : require('./vocabulary_day.js'),
	Vocabulary_week : require('./vocabulary_week.js'),
	Exercise : require('./exercise.js'),
	Users : require('./users.js'),
	Resultspecial : require('./result_special.js'),
	Result : require('./result.js'),
	Day : require('./day.js'),
	Datetime : require('./../library/date.js'),
	View : require('./view.js')
}
