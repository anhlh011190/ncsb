var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var typegrammarChema = new Schema({
	title : String,
	timecreated: String,
	timemodified: String
});
var Typegrammar = mongoose.model('Typegrammar',typegrammarChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var grammar = new Typegrammar(data);
			grammar.save(function(err,grammar_data) {
				if (err) reject(err);
				console.log('Grammar saved successfully');
				resolve(grammar_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Typegrammar.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $lookup:
	            	{
		                from : "grammars",
		                localField : "title",
		                foreignField : "type_grammar",
		                as : "grammars"
		            }
		        },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Typegrammar.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Typegrammar.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,grammar,type){
		return new Promise(function(resolve,reject){
			Typegrammar.update(where,grammar,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}