var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var vocabulary_configChema = new Schema({
	week      : String,
	name_week : String,
	content   : String,
	id        : String,
	timecreated  : String,
	timemodified : String,
	usercreated  : String
});
var Vocabulary_config = mongoose.model('Vocabulary_week',vocabulary_configChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var vocabulary_config = new Vocabulary_config(data);
			vocabulary_config.save(function(err,vocabulary_config_data) {
				if (err) reject(err);
				console.log('Vocabulary_config saved successfully');
				resolve(vocabulary_config_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Vocabulary_config.aggregate([
				{ $match : where },
				{ $sort : 
			     { 
			      id : 1
			     } 
			    },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	//get list đã left join đến bảng vocabulary
	getlists_2 : function(where,start,display){
		return new Promise(function(resolve,reject){
			Vocabulary_config.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $lookup:
	            	{
		                from : "vocabularies",
		                localField : "listvocabulary",
		                foreignField : "id",
		                as : "vocabulary"
		            }
		        },
				{ $skip : start }
           	],function(err, vocabulary_data){
           		if(err) reject(err);
				resolve(vocabulary_data);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Vocabulary_config.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Vocabulary_config.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,vocabulary_config,type){
		return new Promise(function(resolve,reject){
			Vocabulary_config.update(where,vocabulary_config,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}