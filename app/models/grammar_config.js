var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var grammar_configChema = new Schema({
	idgrammar : String,
	day: String, 
	timecreated: String,
	timemodified: String,
	usercreated: String
});
var Grammar_config = mongoose.model('Grammar_config',grammar_configChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var grammar_config = new Grammar_config(data);
			grammar_config.save(function(err,grammar_config_data) {
				if (err) reject(err);
				console.log('Grammar_config saved successfully');
				resolve(grammar_config_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Grammar_config.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	//Get list đã left join đến bảng grammar
	getlists_2 : function(where,start,display){
		return new Promise(function(resolve,reject){
			Grammar_config.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $lookup:
	            	{
		                from : "grammars",
		                localField : "idgrammar",
		                foreignField : "id",
		                as : "grammars"
		            }
		        },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Grammar_config.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Grammar_config.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,grammar_config,type){
		return new Promise(function(resolve,reject){
			Grammar_config.update(where,grammar_config,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}