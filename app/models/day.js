///////////////////////
// Tip học cho ngày  //
///////////////////////

/*
Khai báo thư viện
 */
var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model 
// {title		 : tiêu đê 
// 	day 		 : ngày tiêu đề hiển thị 
// 	timecreated  : thời gian tạo  
// 	timemodified : thời gian chỉnh sửa 
// 	usercreated  : người tạo}
var daysChema = new Schema({
	title : String,
	day: String, 
	timecreated: String,
	timemodified: String,
	usercreated: String,
	videowarmup:String,
	id:String,
	idweek:String
});
//Tạo bảng nếu chưa có hoặc update thêm trường mới nêu thêm vào
var Day = mongoose.model('Day',daysChema);
mongoose.Promise = global.Promise;
//Mặc định giống bình thương với các hàm tạo ,lấy list tiêu để,đếm số lượng tiêu đề,xóa tiêu đê, update tiêu đề mới
module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var phonetics = new Day(data);
			phonetics.save(function(err,phonetics_data) {
				if (err) reject(err);
				resolve(phonetics_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Day.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Day.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Day.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,phonetics,type){
		return new Promise(function(resolve,reject){
			Day.update(where,phonetics,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	//Get list chủ để ngày cộng tuần
	getlists_withday : function(where,start,display){
		return new Promise(function(resolve,reject){
			Day.aggregate([
				{ $match : where },
				{ $sort : 
			     { 
			      week : 1
			     } 
			    },
			    { $lookup:
	            	{
		                from : "vocabulary_weeks",
		                localField : "idweek",
		                foreignField : "id",
		                as : "vocabulary_week"
		            }
		        },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
}