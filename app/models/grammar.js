var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var grammarsChema = new Schema({
	id: String,
	stt : String,
	type_grammar : String,
	file_upload : String,
	title: String, 
	content: String,
	timecreated: String,
	timemodified: String,
	usercreated: String
});
var Grammar = mongoose.model('Grammar',grammarsChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var grammar = new Grammar(data);
			grammar.save(function(err,grammar_data) {
				if (err) reject(err);
				console.log('Grammar saved successfully');
				resolve(grammar_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Grammar.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start },
				{ $sort : 
			     { 
			      stt : 1
			     } 
			    },
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	getlist_all : function(where,start,display){
		return new Promise(function(resolve,reject){
			Grammar.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $lookup:
	            	{
		                from : "typegrammars",
		                localField : "type",
		                foreignField : "title",
		                as : "types"
		            }
		        },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Grammar.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Grammar.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,grammar,type){
		return new Promise(function(resolve,reject){
			Grammar.update(where,grammar,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}