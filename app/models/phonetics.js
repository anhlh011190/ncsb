var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var phoneticsChema = new Schema({
	id: String,
	title : String,
	video_link: String, 
	timecreated: String,
	timemodified: String,
	usercreated: String
});
var Phonetics = mongoose.model('Phonetics',phoneticsChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var phonetics = new Phonetics(data);
			phonetics.save(function(err,phonetics_data) {
				if (err) reject(err);
				console.log('Phonetics saved successfully');
				resolve(phonetics_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Phonetics.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Phonetics.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Phonetics.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,phonetics,type){
		return new Promise(function(resolve,reject){
			Phonetics.update(where,phonetics,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}