var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var usersChema = new Schema({
	id: String,
	roleid : String,
	timecreated: String,
	timemodified: String,
	local:Object
});
var Users = mongoose.model('Users',usersChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var users = new Users(data);
			users.save(function(err,phonetics_data) {
				if (err) reject(err);
				console.log('Users saved successfully');
				resolve(phonetics_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Users.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Users.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Users.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,phonetics,type){
		return new Promise(function(resolve,reject){
			Users.update(where,phonetics,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}