/////////////////////////////
// Load thư viện cần thiết //
/////////////////////////////
var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

/////////////////////////////
// Set up a mongoose model //
/////////////////////////////
var exercisesChema = new Schema({
	id:String,
	idparent: String,
	type : String,
	image : String,
	audio : String,
	content: String, 
	timecreated: String,
	timemodified: String,
	usercreated: String
});
var Exercise = mongoose.model('Exercise',exercisesChema);
mongoose.Promise = global.Promise;

//////////////
//Tạo modle //
//////////////
module.exports = {
	/*
	Tạo câu hỏi mới
	 */
	create : function(data){
		return new Promise(function(resolve,reject){
			var exercise = new Exercise(data);
			exercise.save(function(err,exercises_data) {
				if (err) reject(err);
				console.log('Exercise saved successfully');
				resolve(exercises_data);
			});
		});
	},
	/*
	Lấy list câu hỏi theo tiêu chí type mặc định đã được ex: type = voca/phonetic/grammar
	 */
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Exercise.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	/*
	Lấy list phát âm 
	 */
	getlists_phonetics : function(where,start,display){
		return new Promise(function(resolve,reject){
			Exercise.aggregate([
				{ $match : where },
				{ $lookup:
	            	{
		                from : "phonetics",
		                localField : "idparent",
		                foreignField : "id",
		                as : "phonetics"
		            }
		        },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	/*
	Lấy list garmmars
	 */
	getlists_grammars : function(where,start,display){
		return new Promise(function(resolve,reject){
			Exercise.aggregate([
				{ $match : where },
				{ $lookup:
	            	{
		                from : "grammars",
		                localField : "idparent",
		                foreignField : "id",
		                as : "grammars"
		            }
		        },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	/*
	Lấy list câu hỏi của tất cả các vocabulary theo id trong list có sẵn
	 */
	getlists_vocabularies : function(where,start,display){
		return new Promise(function(resolve,reject){
			Exercise.aggregate([
				{ $match : { idparent : { $in :where} } },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	/*
	Lấy list câu hỏi sử dụng cho việc học viên quay lại làm học lấy câu hỏi mới
	 */
	getlists_2 : function(where,start,display){
		return new Promise(function(resolve,reject){
			Exercise.aggregate([
				{ $match : { id : { $in :where} } },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	/*
	Đếm sô lượng câu hỏi 
	 */
	count : function(where){
		return new Promise(function(resolve,reject){
			Exercise.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	/*
	Xóa câu hỏi 
	 */
	delete : function(where){
		return new Promise(function(resolve,reject){
			Exercise.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	/*
	update câu hỏi
	 */
	update : function(where,phonetics,type){
		return new Promise(function(resolve,reject){
			Exercise.update(where,phonetics,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	/*
	Tìm list câu hỏi voca để show hiển thị phân vocabulary where like
	 */
	getlist_voca : function (data) {
		return new Promise(function(resolve,reject){
			Exercise.find({'type' : new RegExp(data, 'i')},function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}