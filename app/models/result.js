var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var resultChema = new Schema({
	userid: String,
	content : String,
	id_exercise:String,
	result:String,
	point:Number,
	timecreated: Number,
	status:Number,
	vocabulary:Number,
	phonetic:Number,
	grammar:Number,
	day:String
});
var Result = mongoose.model('Result',resultChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var result = new Result(data);
			result.save(function(err,phonetics_data) {
				if (err) reject(err);
				console.log('Result saved successfully');
				resolve(phonetics_data);
			});
		});
	},
	getlists : function(date,userid,timecreated,status,start,display){
		return new Promise(function(resolve,reject){
			Result.aggregate([
				{ $match : { 
					// timecreated : { $gt : timecreated} ,
					userid : userid,
					status: status,
					day:date
					} 
				},
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Result.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Result.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,phonetics,type){
		return new Promise(function(resolve,reject){
			Result.update(where,phonetics,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}