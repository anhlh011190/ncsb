var mongoose = require('mongoose');
var Promise = require("bluebird");
var Schema = mongoose.Schema;

// set up a mongoose model
var phonetics_configChema = new Schema({
	idphonetic : String,
	day: String, 
	timecreated: String,
	timemodified: String,
	usercreated: String
});
var Phonetics_config = mongoose.model('Phonetics_config',phonetics_configChema);
mongoose.Promise = global.Promise;

module.exports = {
	create : function(data){
		return new Promise(function(resolve,reject){
			var phonetics_config = new Phonetics_config(data);
			phonetics_config.save(function(err,phonetics_config_data) {
				if (err) reject(err);
				console.log('Phonetics_config saved successfully');
				resolve(phonetics_config_data);
			});
		});
	},
	getlists : function(where,start,display){
		return new Promise(function(resolve,reject){
			Phonetics_config.aggregate([
				{ $match : where },
				{ $limit : display },
				{ $skip : start }
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	getlists_2 : function(where,start,display){
		return new Promise(function(resolve,reject){
			Phonetics_config.aggregate([
				{ $match : where },
				{ $lookup:
	            	{
		                from : "phonetics",
		                localField : "idphonetic",
		                foreignField : "id",
		                as : "phonetics"
		            }
		        },
				{ $limit : display },
				{ $skip : start },
           	],function(err, users){
           		if(err) reject(err);
				resolve(users);
           	});
		});
	},
	count : function(where){
		return new Promise(function(resolve,reject){
			Phonetics_config.find(where).count(function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	},
	delete : function(where){
		return new Promise(function(resolve,reject){
			Phonetics_config.remove(where, function(err, status){
				if(err) reject(err);
				resolve(status);
			});
		});
	},
	update : function(where,phonetics_config,type){
		return new Promise(function(resolve,reject){
			Phonetics_config.update(where,phonetics_config,type,function(err, total) {
				if(err) reject(err);
				resolve(total);
			});
		});
	}
}