var models = require("./../models/load");
var Users = models.Users;
var Exercise = models.Exercise;
var ObjectId = require('mongodb').ObjectID;
var bcrypt   = require('bcrypt-nodejs');

module.exports = function(app, passport ) {
  //Hiển thị list thành viên
  app.get('/admin/users',isAdmin, function(req, res) {
      Users.getlists({},0,1000).then(function(users){
        var data = {
          title: "User",
          layout: 'backend/admin',
          control:{'level1':'user'},
          users:users
        };
        res.render('backend/user/list', data);
      });
  });
  //add new thành viên
  app.get('/admin/users/add',isAdmin, function(req, res) {
    var data = {
      title: "User",
      layout: 'backend/admin',
      control:{'level1':'user'}
    };
    res.render('backend/user/add', data);
  });
  app.post('/admin/users' ,isAdmin, function(req, res) {
   	var d = new Date();
	  switch(req.body.type) {
      case "add":
        var user_info = {
          roleid:0,
          timecreated: Math.floor(d.getTime()/1000),
          timemodified: Math.floor(d.getTime()/1000),
          local:{
            email: req.body.username,
            password:bcrypt.hashSync("topica@123", bcrypt.genSaltSync(8), null),
            fullname:req.body.fullname
          }
        }
        Users.create(user_info).then(function(users){
          Users.update({"_id":ObjectId(users._id)},{"id":""+users._id},{upsert:true}).then(function(){
            res.redirect('/admin/users')
          });
        });
        break;
      default :
        res.write("0");
        res.end();
        break;
    }
  });
};
function isAdmin(req, res, next) {
    return next();
}
