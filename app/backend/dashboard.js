var models = require("./../models/load");
var Grammar = models.Grammar;
var Grammar_config = models.Grammar_config;
var ObjectId = require('mongodb').ObjectID;
var Exercise = models.Exercise;
module.exports = function(app, passport) {
	//list câu hỏi của grammar
  app.get('/admin',isAdmin, function(req, res) {
    var data = {
      title: "Dashboard",
      layout: 'backend/admin',
      control:{'level1':'dashboard'}
    };
    res.render('backend/dashboard/index', data);
  });
};
function isAdmin(req, res, next) {
    return next();
}
