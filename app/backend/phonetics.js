var fs         = require('fs');

var models = require("./../models/load");
var Phonetics = models.Phonetics;
var Phonetics_config = models.Phonetics_config;
var Exercise = models.Exercise;
var ObjectId = require('mongodb').ObjectID;

module.exports = function(app, passport , multipartMiddleware) {
  //Hiển thị list câu hỏi của phonetics
  app.get('/admin/phonetics/exercises',isAdmin, function(req, res) {
      Exercise.getlists_phonetics({"type":"phonetics"},0,1000).then(function(exercises){
        var data = {
          title: "Phonetics",
          layout: 'backend/admin',
          control:{'level1':'phonetics'},
          exercises:exercises
        };
        res.render('backend/phonetics/exercises', data);
      });
  });

  //Thêm câu hỏi của phonetics
  app.get('/admin/phonetics/exercises/edit',isAdmin, function(req, res) {
    Exercise.getlists({"_id":ObjectId(req.query.id)},0,1).then(function(exercises){
      if(exercises.length == 1){
        Phonetics.getlists({},0,1000).then(function(phonetics){
          var data = {
            title: "Phonetics",
            layout: 'backend/admin',
            control:{'level1':'phonetics'},
            exercises: exercises[0],
            phonetics:phonetics
          };
          res.render('backend/phonetics/exercise_edit', data);
        });
      }else{
        res.redirect('/admin/phonetics/exercises')
      }
        
    });
  });
  
  //Sửa yêu câu hỏi của phonetics
  app.get('/admin/phonetics/exercises/add',isAdmin, function(req, res) {
      Phonetics.getlists({},0,1000).then(function(phonetics){
        var data = {
          title: "Phonetics",
          layout: 'backend/admin',
          control:{'level1':'phonetics'},
          phonetics:phonetics
        };
        res.render('backend/phonetics/exercise_add', data);
      });
  });

  app.get('/admin/phonetics/calendar',isAdmin, function(req, res) {
    var d = new Date();
    var year = d.getFullYear();
    var month = parseInt(d.getMonth()+1);
    if(month < 10){
      month = "0"+month
    }
    var day = d.getDate();
    if(day < 10){
      day = "0"+day
    }
    var date = year+'-'+month+"-"+day;
    Phonetics_config.getlists_2({},0,1000).then(function(phonetics_config){
      Phonetics.getlists({},0,1000).then(function(phonetics){
        var data = {
          title: "Phonetics",
          layout: 'backend/admin',
          control:{'level1':'phonetics'},
          phonetics_config: phonetics_config,
          phonetics:phonetics,
          day_current:date
        };
        res.render('backend/phonetics/calendar', data);
      });
    });
  });
  app.get('/admin/phonetics/calendar/edit',isAdmin, function(req, res) {
    Phonetics_config.getlists_2({"_id":ObjectId(req.query.id)},0,1000).then(function(phonetics_config){
      Phonetics.getlists({},0,1000).then(function(phonetics){
        var data = {
          title: "Phonetics",
          layout: 'backend/admin',
          control:{'level1':'phonetics'},
          phonetics_config: phonetics_config[0],
          phonetics:phonetics
        };
        res.render('backend/phonetics/calendar_edit', data);
      });
    });
  });
  app.get('/admin/phonetics/calendar/delete',isAdmin, function(req, res) {
    Phonetics_config.delete({"_id":ObjectId(req.query.id)}).then(function(){
        res.redirect('/admin/phonetics/calendar')
    });
  }); 
  app.get('/admin/phonetics/add',isAdmin, function(req, res) {
    var data = {
      title: "Phonetics",
      layout: 'backend/admin',
      control:{'level1':'phonetics'}
    };
    res.render('backend/phonetics/add', data);
  }); 
  app.get('/admin/phonetics/edit',isAdmin, function(req, res) {
    Phonetics.getlists({"_id":ObjectId(req.query.id)},0,1).then(function(phonetics){
      if(phonetics.length == 1){
        var data = {
          title: "Phonetics",
          layout: 'backend/admin',
          phonetics:phonetics[0],
          control:{'level1':'phonetics'}
        };
        res.render('backend/phonetics/edit', data);
      }else{
        res.redirect('/admin/phonetics')
      }
    });    
  }); 
  app.get('/admin/phonetics',isAdmin, function(req, res) {
    Phonetics.getlists({},0,100).then(function(phonetics){
      var data = {
        title: "Phonetics",
        layout: 'backend/admin',
        phonetics:phonetics,
        control:{'level1':'phonetics'}
      };
      res.render('backend/phonetics/list', data);
    });
  });
  app.get('/admin/phonetics/delete',isAdmin, function(req, res) {
    Phonetics.delete({"_id":ObjectId(req.query.id)}).then(function(){
      res.redirect('/admin/phonetics')
    });
  });
  app.get('/admin/phonetics/exercises/delete',isAdmin, function(req, res) {
    Exercise.delete({"_id":ObjectId(req.query.id)}).then(function(){
      res.redirect('/admin/phonetics/exercises')
    });
  });
  app.post('/admin/phonetics',multipartMiddleware ,isAdmin, function(req, res) {
   	var d = new Date();
        console.log(req.body)
	  switch(req.body.type) {
      //Xử lý thêm câu hỏi của phonetics
      case 'exercise_add':
        var content = {
          "content":req.body.question,
          "option1":req.body.option1,
          "option2":req.body.option2,
          "option3":req.body.option3,
          "option4":req.body.option4,
          "answer":req.body.answer,
          "giaithich":req.body.giaithich
        }
        exercise_info = {
          content: JSON.stringify(content),
          type: "phonetics",
          timecreated: Math.floor(d.getTime()/1000),
          timemodified: Math.floor(d.getTime()/1000),
          usercreated:"admin",
          idparent:req.body.idparent
        }
        Exercise.create(exercise_info).then(function(exercise){
          Exercise.update({"_id":ObjectId(exercise._id)},{"id":""+exercise._id},{upsert:true}).then(function(){
            var file_audio              = req.files.audio;
            var originalFilename_audio  = file_audio.name;
            var fileType_audio          = file_audio.type.split('/')[1];
            var pathUpload_audio        = 'public/uploads/phonetics/audio/' + exercise._id+'.'+fileType_audio;
            fs.readFile(file_audio.path, function(err, dataaudio) {
              if(!err) {
                fs.writeFile(pathUpload_audio, dataaudio, function() {
                  var file_image              = req.files.image;
                  var originalFilename_image  = file_image.name;
                  var fileType_image          = file_image.type.split('/')[1];
                  var pathUpload_image        = 'public/uploads/phonetics/images/' + exercise._id+'.'+fileType_image;
                  fs.readFile(file_image.path, function(err, dataimage) {
                    if(!err) {
                      fs.writeFile(pathUpload_image, dataimage, function() {
                        console.log(fileType_image)
                        if(fileType_image == 'jpeg'){
                          var content = {
                            "content": req.body.question,
                            "option1": req.body.option1,
                            "option2": req.body.option2,
                            "option3": req.body.option3,
                            "option4": req.body.option4,
                            "answer": req.body.answer,
                            "giaithich": req.body.giaithich,
                            "image": 1
                          }
                          exercise_info = {
                            content: JSON.stringify(content)
                          }
                          Exercise.update({"_id":ObjectId(exercise._id)},exercise_info,{}).then(function(){
                            res.redirect('/admin/phonetics/exercises')
                          });
                        }else{
                          res.redirect('/admin/phonetics/exercises')
                        }
                      });
                    }
                  });
                });
              }
            });
          });
        });
          
        break;
      //Xử lý sửa câu hỏi của phonetics
      case 'exercise_edit':
        Exercise.count({"_id":ObjectId(req.body.id)}).then(function(count){
          if(count == 1){
            var content = {
              "content":req.body.question,
              "option1":req.body.option1,
              "option2":req.body.option2,
              "option3":req.body.option3,
              "option4":req.body.option4,
              "answer":req.body.answer,
              "giaithich":req.body.giaithich
            }
            exercise_info = {
              content: JSON.stringify(content),
              type: "phonetics",
              timemodified: Math.floor(d.getTime()/1000),
              idparent:req.body.idparent
            }
            Exercise.update({"_id":ObjectId(req.body.id)},exercise_info,{upsert:true}).then(function(status){
              var file_audio              = req.files.audio;
              var originalFilename_audio  = file_audio.name;
              var fileType_audio          = file_audio.type.split('/')[1];
              var pathUpload_audio        = 'public/uploads/phonetics/audio/' + req.body.id+'.'+fileType_audio;
              fs.readFile(file_audio.path, function(err, dataaudio) {
                if(!err) {
                  fs.rmdir(pathUpload_audio,function(){
                    fs.writeFile(pathUpload_audio, dataaudio, function() {
                      var file_image              = req.files.image;
                      var originalFilename_image  = file_image.name;
                      var fileType_image          = file_image.type.split('/')[1];
                      var pathUpload_image        = 'public/uploads/phonetics/images/' + req.body.id+'.'+fileType_image;
                      fs.readFile(file_image.path, function(err, dataimage) {
                        if(!err) {
                          fs.rmdir(pathUpload_image,function(){
                            fs.writeFile(pathUpload_image, dataimage, function() {
                              console.log(fileType_image)
                              if(fileType_image == 'jpeg'){
                                var content = {
                                  "content": req.body.question,
                                  "option1": req.body.option1,
                                  "option2": req.body.option2,
                                  "option3": req.body.option3,
                                  "option4": req.body.option4,
                                  "answer": req.body.answer,
                                  "giaithich": req.body.giaithich,
                                  "image": 1
                                }
                                exercise_info = {
                                  content: JSON.stringify(content)
                                }
                                Exercise.update({"_id":ObjectId(req.body.id)},exercise_info,{}).then(function(){
                                  res.redirect('/admin/phonetics/exercises')
                                });
                              }else{
                                res.redirect('/admin/phonetics/exercises')
                              }
                            });
                          });
                        }
                      });
                      res.redirect('/admin/phonetics/exercises')
                    });
                  });
                }
              });
            });
          }else{
            res.redirect('/admin/phonetics/exercises')
          }
        }); 
        break;
      case 'add':
        phonetics_info = {
          title: req.body.title,
          video_link: req.body.video_link,
          timecreated: Math.floor(d.getTime()/1000),
          timemodified: Math.floor(d.getTime()/1000),
          usercreated:"admin"
        }
        Phonetics.create(phonetics_info).then(function(phonetics){
          Phonetics.update({"_id":ObjectId(phonetics._id)},{"id":""+phonetics._id},{upsert:true}).then(function(){
            res.redirect('/admin/phonetics')
          });
        });
        break;
      case 'calendar':
        Phonetics_config.count({"day":req.body.calendar}).then(function(count){
          if(count == 1){
            phonetics_config_info = {
              idphonetic: req.body.idphonetic,
              timemodified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Phonetics_config.update({"day":req.body.calendar},phonetics_config_info,{upsert:true}).then(function(){
              res.redirect('/admin/phonetics/calendar')
            });
          }else{
            phonetics_config_info = {
              day: req.body.calendar,
              idphonetic: req.body.idphonetic,
              timecreated: Math.floor(d.getTime()/1000),
              timemodified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Phonetics_config.create(phonetics_config_info).then(function(){
              res.redirect('/admin/phonetics/calendar')
            });
          }
        });
        break;
      case 'calendar_edit':
        Phonetics_config.count({"day":req.body.calendar}).then(function(count){
          if(count == 1){
            phonetics_config_info = {
              idphonetic: req.body.idphonetic,
              timemodified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Phonetics_config.update({"day":req.body.calendar},phonetics_config_info,{upsert:true}).then(function(){
              res.redirect('/admin/phonetics/calendar')
            });
          }else{
            res.redirect('/admin/phonetics/calendar')
          }
        });
        break;
      case 'edit':
        Phonetics.count({"_id":ObjectId(req.body.id)}).then(function(count){
          if(count == 1){
            phonetics_info = {
              title: req.body.title,
              video_link: req.body.video_link,
              timemodified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Phonetics.update({"_id":ObjectId(req.body.id)},phonetics_info,{upsert:true}).then(function(){
              res.redirect('/admin/phonetics')
            });
          }else{
            res.redirect('/admin/phonetics')
          }
        });
        break;
      default :
        res.write("0");
        res.end();
        break;
    }
  });
};
function isAdmin(req, res, next) {
    return next();
}

