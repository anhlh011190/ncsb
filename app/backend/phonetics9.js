var models = require("./../models/load");
var Phonetics = models.Phonetics;
var Phonetics_config = models.Phonetics_config;
var Exercise = models.Exercise;
var ObjectId = require('mongodb').ObjectID;
var fileUpload = require('express-fileupload');

app.get('/admin/phonetics/exercises', function(req, res) {
    Exercise.getlists({},0,1000).then(function(exercises){
      var data = {
        title: "Phonetics",
        layout: 'backend/admin',
        control:{'level1':'phonetics'},
        exercises:exercises
      };
      res.render('backend/phonetics/exercises', data);
    });
}),

//Thêm câu hỏi của phonetics
app.get('/admin/phonetics/exercises/edit', function(req, res) {
  Phonetics_config.getlists_2({"_id":ObjectId(req.query.id)},0,1000).then(function(phonetics_config){
    Phonetics.getlists({},0,1000).then(function(phonetics){
      var data = {
        title: "Phonetics",
        layout: 'backend/admin',
        control:{'level1':'phonetics'},
        phonetics_config: phonetics_config[0],
        phonetics:phonetics
      };
      res.render('backend/phonetics/exercise_edit', data);
    });
  });
}),
//Sửa yêu câu hỏi của phonetics
app.get('/admin/phonetics/exercises/add', function(req, res) {
    Phonetics.getlists({},0,1000).then(function(phonetics){
      var data = {
        title: "Phonetics",
        layout: 'backend/admin',
        control:{'level1':'phonetics'},
        phonetics:phonetics
      };
      res.render('backend/phonetics/exercise_add', data);
    });
}),

app.get('/admin/phonetics/calendar', function(req, res) {
  var d = new Date();
  var year = d.getFullYear();
  var month = parseInt(d.getMonth()+1);
  if(month < 10){
    month = "0"+month
  }
  var day = d.getDate();
  if(day < 10){
    day = "0"+day
  }
  var date = year+'-'+month+"-"+day;
  Phonetics_config.getlists_2({},0,1000).then(function(phonetics_config){
    Phonetics.getlists({},0,1000).then(function(phonetics){
      var data = {
        title: "Phonetics",
        layout: 'backend/admin',
        control:{'level1':'phonetics'},
        phonetics_config: phonetics_config,
        phonetics:phonetics,
        day_current:date
      };
      res.render('backend/phonetics/calendar', data);
    });
  });
}),
app.get('/admin/phonetics/calendar/edit', function(req, res) {
  Phonetics_config.getlists_2({"_id":ObjectId(req.query.id)},0,1000).then(function(phonetics_config){
    Phonetics.getlists({},0,1000).then(function(phonetics){
      var data = {
        title: "Phonetics",
        layout: 'backend/admin',
        control:{'level1':'phonetics'},
        phonetics_config: phonetics_config[0],
        phonetics:phonetics
      };
      res.render('backend/phonetics/calendar_edit', data);
    });
  });
}),
app.get('/admin/phonetics/calendar/delete', function(req, res) {
  Phonetics_config.delete({"_id":ObjectId(req.query.id)}).then(function(){
      res.redirect('/admin/phonetics/calendar')
  });
}),
app.get('/admin/phonetics/add', function(req, res) {
  var data = {
    title: "Phonetics",
    layout: 'backend/admin',
    control:{'level1':'phonetics'}
  };
  res.render('backend/phonetics/add', data);
}),
app.get('/admin/phonetics/edit', function(req, res) {
  Phonetics.getlists({"_id":ObjectId(req.query.id)},0,1).then(function(phonetics){
    if(phonetics.length == 1){
      var data = {
        title: "Phonetics",
        layout: 'backend/admin',
        phonetics:phonetics[0],
        control:{'level1':'phonetics'}
      };
      res.render('backend/phonetics/edit', data);
    }else{
      res.redirect('/admin/phonetics')
    }
  });    
}),
app.get('/admin/phonetics', function(req, res) {
  Phonetics.getlists({},0,100).then(function(phonetics){
    var data = {
      title: "Phonetics",
      layout: 'backend/admin',
      phonetics:phonetics,
      control:{'level1':'phonetics'}
    };
    res.render('backend/phonetics/list', data);
  });
}),
app.get('/admin/phonetics/delete', function(req, res) {
  Phonetics.delete({"_id":ObjectId(req.query.id)}).then(function(){
    res.redirect('/admin/phonetics')
  });
}),
app.post('/admin/phonetics', function(req, res) {
  var d = new Date();
      console.log(req.body)
  switch(req.body.type) {
    case 'exercise_add':
      app.use(fileUpload());
      // var content = {
      //   "option1":req.body.option1,
      //   "option2":req.body.option2,
      //   "option3":req.body.option3,
      //   "option4":req.body.option4
      // }
      // exercise_info = {
      //   content: JSON.stringify(content),
      //   type: "phonetics",
      //   timeCreated: Math.floor(d.getTime()/1000),
      //   timeModified: Math.floor(d.getTime()/1000),
      //   usercreated:"admin",
      //   idparent:req.body.idparent
      // }
      // // var sampleFile;
      // // sampleFile = req.files.audio;
      // console.log(exercise_info)
      res.write("A");
      res.end();
      // Exercise.create(exercise_info).then(function(exercise){
      //   var sampleFile;
      //   sampleFile = req.files.audio;
      //   if(typeof sampleFile !== 'undefined'){
      //     sampleFile.mv('./public/uploads/'+slide._id+'.jpg', function(err) {
      //       res.redirect('/admin/phonetics')
      //     });
      //   }
      //   res.redirect('/admin/phonetics')
      // });
      break;
    case 'add':
      phonetics_info = {
        title: req.body.title,
        video_link: req.body.video_link,
        timeCreated: Math.floor(d.getTime()/1000),
        timeModified: Math.floor(d.getTime()/1000),
        usercreated:"admin"
      }
      Phonetics.create(phonetics_info).then(function(phonetics){
        Phonetics.update({"_id":ObjectId(phonetics._id)},{"id":""+phonetics._id},{upsert:true}).then(function(){
          res.redirect('/admin/phonetics')
        });
      });
      break;
    case 'calendar':
      Phonetics_config.count({"day":req.body.calendar}).then(function(count){
        if(count == 1){
          phonetics_config_info = {
            idphonetic: req.body.idphonetic,
            timeModified: Math.floor(d.getTime()/1000),
            usercreated:"admin"
          }
          Phonetics_config.update({"day":req.body.calendar},phonetics_config_info,{upsert:true}).then(function(){
            res.redirect('/admin/phonetics/calendar')
          });
        }else{
          phonetics_config_info = {
            day: req.body.calendar,
            idphonetic: req.body.idphonetic,
            timeCreated: Math.floor(d.getTime()/1000),
            timeModified: Math.floor(d.getTime()/1000),
            usercreated:"admin"
          }
          Phonetics_config.create(phonetics_config_info).then(function(){
            res.redirect('/admin/phonetics/calendar')
          });
        }
      });
      break;
    case 'calendar_edit':
      Phonetics_config.count({"day":req.body.calendar}).then(function(count){
        if(count == 1){
          phonetics_config_info = {
            idphonetic: req.body.idphonetic,
            timeModified: Math.floor(d.getTime()/1000),
            usercreated:"admin"
          }
          Phonetics_config.update({"day":req.body.calendar},phonetics_config_info,{upsert:true}).then(function(){
            res.redirect('/admin/phonetics/calendar')
          });
        }else{
          res.redirect('/admin/phonetics/calendar')
        }
      });
      break;
    case 'edit':
      Phonetics.count({"_id":ObjectId(req.body.id)}).then(function(count){
        if(count == 1){
          phonetics_info = {
            title: req.body.title,
            video_link: req.body.video_link,
            timeModified: Math.floor(d.getTime()/1000),
            usercreated:"admin"
          }
          Phonetics.update({"_id":ObjectId(req.body.id)},phonetics_info,{upsert:true}).then(function(){
            res.redirect('/admin/phonetics')
          });
        }else{
          res.redirect('/admin/phonetics')
        }
      });
      break;
    default :
      res.write("0");
      res.end();
      break;
  }
}),
module.exports = app;