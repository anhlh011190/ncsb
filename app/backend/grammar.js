var models = require("./../models/load");
var fs         = require('fs');
var Grammar = models.Grammar;
var Grammar_config = models.Grammar_config;
var ObjectId = require('mongodb').ObjectID;
var Exercise = models.Exercise;
module.exports = function(app, passport,multipartMiddleware) {
	//list câu hỏi của grammar
  app.get('/admin/grammar/exercises',isAdmin, function(req, res) {
      Exercise.getlists_grammars({"type":"grammar"},0,1000).then(function(exercises){
        console.log(exercises)
        var data = {
          title: "Grammar",
          layout: 'backend/admin',
          control:{'level1':'grammar'},
          exercises:exercises
        };
        res.render('backend/grammar/exercises', data);
      });
  });
  //xóa nội dung học theo ngày của grammmar
  app.get('/admin/grammar/calendar/delete',isAdmin, function(req, res) {
    Grammar_config.delete({"_id":ObjectId(req.query.id)}).then(function(){
        res.redirect('/admin/grammar/calendar')
    });
  }); 

  //sửa câu hỏi của grammar
 app.get('/admin/grammar/exercises/edit',isAdmin, function(req, res) {
    Exercise.getlists({"_id":ObjectId(req.query.id)},0,1).then(function(exercises){
      if(exercises.length == 1){
        Grammar.getlists({},0,1000).then(function(grammar){
          var data = {
            title: "Grammar",
            layout: 'backend/admin',
            control:{'level1':'grammar'},
            exercises: exercises[0],
            grammar:grammar
          };
          res.render('backend/grammar/exercise_edit', data);
        });
      }else{
        res.redirect('/admin/grammar/exercises')
      }
        
    });
  });
 //thêm câu hỏi của grammar
  app.get('/admin/grammar/exercises/add',isAdmin, function(req, res) {
      Grammar.getlists({},0,1000).then(function(grammar){
        var data = {
          title: "Grammar",
          layout: 'backend/admin',
          control:{'level1':'grammar'},
          grammar:grammar
        };
        res.render('backend/grammar/exercise_add', data);
      });
  });
  //config nội dung học theo ngày của grammar
  app.get('/admin/grammar/calendar',isAdmin, function(req, res) {
    var d = new Date();
    var year = d.getFullYear();
    var month = parseInt(d.getMonth()+1);
    if(month < 9){
      month = "0"+month
    }
    var day = d.getDate();
    if(day < 9){
      day = "0"+day
    }
    var date = year+'-'+month+"-"+day;
    Grammar_config.getlists_2({},0,1000).then(function(grammar_config){
      
      Grammar.getlists({},0,1000).then(function(grammar){
        // console.log(grammar_config[2].grammars);
        // console.log(grammar_config[2].grammars[0].title)
        var data = {
          title: "Grammar",
          layout: 'backend/admin',
          control:{'level1':'grammar'},
          grammar_config: grammar_config,
          grammar:grammar,
          day_current:date
        };
        res.render('backend/grammar/calendar', data);
      });
    });
  }); 
  //sửa nội dung học theo ngày trong grammar
   app.get('/admin/grammar/calendar/edit',isAdmin, function(req, res) {
      Grammar_config.getlists_2({"_id":ObjectId(req.query.id)},0,1000).then(function(grammar_config){
        Grammar.getlists({},0,1000).then(function(grammar){
          var data = {
            title: "Grammar",
            layout: 'backend/admin',
            control:{'level1':'grammar'},
            grammar_config: grammar_config[0],
            grammar:grammar
          };
          res.render('backend/grammar/calendar_edit', data);
        });
      });
  });
   //xóa bài tập của grammar
  app.get('/admin/grammar/exercises/delete',isAdmin, function(req, res) {
    Exercise.delete({"_id":ObjectId(req.query.id)}).then(function(){
      res.redirect('/admin/grammar/exercises')
    });
  });
  //thêm nội dung học grammar
  app.get('/admin/grammar/add',isAdmin, function(req, res) {
    var data = {
      title: "Grammar",
      type_grammar:"Constant.type_grammar",
      layout: 'backend/admin',
      control:{'level1':'grammar'}
    };
    res.render('backend/grammar/add', data);
  }); 
  //sua noi dung hoc grammar
  app.get('/admin/grammar/edit',isAdmin, function(req, res) {
    Grammar.getlists({"_id":ObjectId(req.query.id)},0,1).then(function(grammar){
      if(grammar.length == 1){
        var data = {
          title: "Grammar",
          type_grammar:"Constant.type_grammar",
          layout: 'backend/admin',
          grammar:grammar[0],
          control:{'level1':'grammar'}
        };
        res.render('backend/grammar/edit', data);
      }else{
        res.redirect('/admin/grammar')
      }
    });    
  }); 
  //tra ve list cac noi dung hoc trong grammar
  app.get('/admin/grammar',isAdmin, function(req, res) {
    Grammar.getlists({},0,100).then(function(grammar){
      var data = {
        title: "Grammar",
        type_grammar:"Constant.type_grammar",
        layout: 'backend/admin',
        grammar:grammar,
        control:{'level1':'grammar'}
      };
      res.render('backend/grammar/list', data);
    });
  });
  //xoa noi dung hoc cua grammar
  app.get('/admin/grammar/delete',isAdmin, function(req, res) {
    Grammar.delete({"_id":ObjectId(req.query.id)},0,1).then(function(grammar){
      res.redirect('/admin/grammar')
    })
  });
  app.post('/admin/grammar',multipartMiddleware,isAdmin,function(req, res) {
    
    var d = new Date();
    switch(req.body.type) {
      //Xử lý thêm câu hỏi của grammar
      case 'exercise_add':
        var content = {
          "option":req.body.option,
          "option1":req.body.option1,
          "option2":req.body.option2,
          "option3":req.body.option3,
          "option4":req.body.option4,
          "giaithich":req.body.giaithich,
          "answer":req.body.answer,
        }
        exercise_info = {
          content: JSON.stringify(content),
          type: "grammar",
          timecreated: Math.floor(d.getTime()/1000),
          timemodified: Math.floor(d.getTime()/1000),
          usercreated:"admin",
          idparent:req.body.idparent
        }
        Exercise.create(exercise_info).then(function(exercise){
           Exercise.update({"_id":ObjectId(exercise._id)},{"id":""+exercise._id},{upsert:true}).then(function(){
              res.redirect('/admin/grammar/exercises')
            });
        });
        break;
      //Xử lý sửa câu hỏi của grammar
      case 'exercise_edit':
        Exercise.count({"_id":ObjectId(req.body.id)}).then(function(count){
          if(count == 1){
            var content = {
               "option":req.body.option,
                "option1":req.body.option1,
                "option2":req.body.option2,
                "option3":req.body.option3,
                "option4":req.body.option4,
                "giaithich":req.body.giaithich,
                "answer":req.body.answer,
            }
            exercise_info = {
              content: JSON.stringify(content),
              type: "grammar",
              timecreated: Math.floor(d.getTime()/1000),
              timemodified: Math.floor(d.getTime()/1000),
              idparent:req.body.idparent
            }
            Exercise.update({"_id":ObjectId(req.body.id)},exercise_info,{upsert:true}).then(function(exercise){
              res.redirect('/admin/grammar/exercises')
            });
          }else{
            res.redirect('/admin/grammar/exercises')
          }
        }); 
        break;
      case 'add':
            var file_upload = req.files.file_upload;
            var file_name  = file_upload.name;
            var fileSize         = file_upload.size;
            var fileType  = file_upload.type.split('/')[1];
            var pathUpload   = 'public/uploads/grammar/file/' +file_name;
            fs.readFile(file_upload.path, function(err, dataaudio) {
               if(!err) {
                fs.writeFile(pathUpload, dataaudio, function() {
                   fs.readFile(file_upload.path, function(err, dataaudio) {
                    if(!err) {
                        fs.writeFile(pathUpload, dataaudio, function() {
                            grammar_info = {
                                stt : req.body.stt,
                                title: req.body.title,
                                type_grammar: req.body.type_grammar,
                                content: req.body.content,
                                file_upload : 'uploads/grammar/file/' + file_name,
                                timecreated : Math.floor(d.getTime()/1000),
                                timemodified: Math.floor(d.getTime()/1000),
                                usercreated :"admin"
                          };
                          Grammar.create(grammar_info).then(function(grammar){
                              Grammar.update({"_id":ObjectId(grammar._id)},{"id":""+grammar._id},{upsert:true}).then(function(){
                                    res.redirect('/admin/grammar')
                               });
                          });
                      });
                    }
                });
            });
          }
        });
        break;
      case 'calendar':
        Grammar_config.count({"day":req.body.calendar}).then(function(count){
          if(count == 1){

     
            grammar_config_info = {
              idgrammar: req.body.idgrammar,
              timeModified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Grammar_config.update({"day":req.body.calendar},grammar_config_info,{upsert:true}).then(function(){
              res.redirect('/admin/grammar/calendar')
            });
          }else{
            grammar_config_info = {
              day: req.body.calendar,
              idgrammar: req.body.idgrammar,
              timeCreated: Math.floor(d.getTime()/1000),
              timeModified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Grammar_config.create(grammar_config_info).then(function(){
              res.redirect('/admin/grammar/calendar')
            });
          }
        });
          
        break;
         case 'calendar_edit':
        Grammar_config.count({"day":req.body.calendar}).then(function(count){
          if(count == 1){
            grammar_config_info = {
              idgrammar: req.body.idgrammar,
              timeModified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Grammar_config.update({"day":req.body.calendar},grammar_config_info,{upsert:true}).then(function(){
              res.redirect('/admin/grammar/calendar')
            });
          }else{
            res.redirect('/admin/grammar/calendar')
          }
        });
        break;
      case 'edit':
            var file_upload = req.files.file_upload;
            var file_name  = file_upload.name;
            var fileSize         = file_upload.size;
            var fileType  = file_upload.type.split('/')[1];
            var pathUpload   = 'public/uploads/grammar/file/' +file_name;
           
            Grammar.count({"_id":ObjectId(req.body.id)}).then(function(count){
                    
                        fs.readFile(file_upload.path, function(err, data) {
                            if(!err) {
                              fs.writeFile(pathUpload, data, function() {
                                 console.log(fileType);
                                if((fileType == 'octet-stream' || fileType == 'doc' || fileType == 'docx' || fileType == 'msword') &&  file_name != ''){
                                  fs.unlink('public/'+req.body.file_upload_old,function(){
                                    // console.log("a");
                                    grammar_info = {
                                            title: req.body.title,
                                            type_grammar: req.body.type_grammar,
                                            content: req.body.content,
                                            file_upload : 'uploads/grammar/file/' + file_name,
                                            timecreated : Math.floor(d.getTime()/1000),
                                            timemodified: Math.floor(d.getTime()/1000),
                                            usercreated :"admin"
                                    };
                                   
                                    Grammar.update({"_id":ObjectId(req.body.id)},grammar_info,{upsert:true}).then(function(grammar){
                                      res.redirect('/admin/grammar')
                                    });
                                });
                                }else{
                                    grammar_info = {
                                                stt: req.body.stt,
                                                title: req.body.title,
                                                type_grammar: req.body.type_grammar,
                                                content: req.body.content,
                                                timecreated : Math.floor(d.getTime()/1000),
                                                timemodified: Math.floor(d.getTime()/1000),
                                                usercreated :"admin"
                                    };
                                    Grammar.update({"_id":ObjectId(req.body.id)},grammar_info,{upsert:true}).then(function(grammar){
                                        res.redirect('/admin/grammar')
                                    });
                                    
                                }
                              });
                            }
                          });
            });
        break;
    }
  });
};
function isAdmin(req, res, next) {
    return next();
}
