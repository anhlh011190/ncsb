var models = require("./../models/load");
var Day = models.Day;
var Vocabulary_week = models.Vocabulary_week;
var ObjectId = require('mongodb').ObjectID;

module.exports = function(app, passport) {
  //Hiển thị list câu hỏi của phonetics
  app.get('/admin/day',isAdmin, function(req, res) {
    var d = new Date();
    var year = d.getFullYear();
    var month = parseInt(d.getMonth()+1);
    if(month < 10){
      month = "0"+month
    }
    var day = d.getDate();
    if(day < 10){
      day = "0"+day
    }
    var date = year+'-'+month+"-"+day;
    Day.getlists({},0,1000).then(function(days){
      Vocabulary_week.getlists({},0,15).then(function(cdt){
        var data = {
          title: "Today Information",
          layout: 'backend/admin',
          control:{'level1':'day'},
          cdt:cdt,
          days:days,
          day_current:date
        };
        res.render('backend/day/index', data);
      });
    });
  });
  app.get('/admin/day/delete',isAdmin, function(req, res) {
    Day.delete({"_id":ObjectId(req.query.id)}).then(function(){
      res.redirect('/admin/day')
    });
  });
  app.post('/admin/day' ,isAdmin, function(req, res) {
   	var d = new Date();
	  switch(req.body.type) {
      case 'add':
        Day.count({"day":req.body.day}).then(function(count){
          if(count == 1){
            day_info = {
              idweek: req.body.idweek,
              title: req.body.title,
              timemodified: Math.floor(d.getTime()/1000),
              usercreated:"admin",
              videowarmup:req.body.videowarmup
            }
            Day.update({"day":req.body.day},day_info,{upsert:true}).then(function(){
              res.redirect('/admin/day')
            });
          }else{
            day_info = {
              day: req.body.day,
              title: req.body.title,
              idweek: req.body.idweek,
              timecreated: Math.floor(d.getTime()/1000),
              timemodified: Math.floor(d.getTime()/1000),
              usercreated:"admin",
              videowarmup:req.body.videowarmup
            }
            Day.create(day_info).then(function(dayres){
              Day.update({"_id":ObjectId(dayres._id)},{"id":""+dayres._id},{}).then(function(){
                res.redirect('/admin/day')
              });
            });
          }
        });
        break;
      default :
        res.write("0");
        res.end();
        break;
    }
  });
};
function isAdmin(req, res, next) {
    return next();
}