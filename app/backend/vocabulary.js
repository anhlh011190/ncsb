///////////////////////////
// Các thư viện và cần dùng //
///////////////////////////

// Thư viện fs dùng cho upload
var fs                  = require('fs');
var XLSX = require('xlsx');
var workbook = XLSX.readFile('voca.xlsx');

////////////////////////
// Các model cần cùng //
////////////////////////
var models              = require("./../models/load");
var Vocabulary          = models.Vocabulary;
var Vocabulary_config   = models.Vocabulary_config;
var Vocabulary_day      = models.Vocabulary_day;
var Vocabulary_week     = models.Vocabulary_week;
var Day_topic           = models.Day;
var Exercise            = models.Exercise;

/*
Dùng cho việc trừu tượng hóa Object hóa id
 */
var ObjectId            = require('mongodb').ObjectID;
//Dùng cho việc lấy giờ và ngày hiện UNIXTIME
var d = new Date();
/**
 * [get_keyword Hàm cắt chuỗi lấy key word trong listvocabulary]
 * @param  {[type]} vocabulary [description]
 * @return {[type]} keyword   [description]
 */
function get_keyword(vocabulary){
  var list = vocabulary;
  var string_end = list.indexOf(",",40);
  var keyword = list.substring(44,string_end-1 );
  return keyword;
}
/**
 * Hàm lấy năm tháng ngày hiện tại
 * @return {date} năm tháng ngày hiện tại
 */
function get_date_now(){
    var d = new Date();
    var year = d.getFullYear();
    var month = parseInt(d.getMonth()+1);
    if(month < 10){
      month = "0"+month
    }
    var day = d.getDate();
    if(day < 10){
      day = "0"+day
    }
    var date = year+'-'+month+"-"+day;

    return date;
}
/////////////////////////
// EXPORT RA CÁC ROUTE //
/////////////////////////
module.exports = function(app, passport,multipartMiddleware) {
  /////////////////////
  // Quản lý từ vựng //
  /////////////////////
  /*
  Trang quản lý các từ vựng đã có
   */
  app.get('/admin/vocabulary',isAdmin, function(req, res) {
    Vocabulary.getlists({},0,100).then(function(vocabulary){
      var data = {
        title: "Vocabulary",
        layout: 'backend/admin',
        vocabulary:vocabulary,
        control:{'level1':'vocabulary'}
      };
      res.render('backend/vocabulary/list', data);
    });
  });

  /*
  Hiển thị form add từ vưng 
   */
  app.get('/admin/vocabulary/add',isAdmin, function(req, res) {
    var data = {
      title: "Add a vocabulary",
      layout: 'backend/admin',
      control:{'level1':'vocabulary'}
    };
    res.render('backend/vocabulary/add', data);
  });

  /*
  Hiển thị form edit từ vựng
   */
  app.get('/admin/vocabulary/edit',isAdmin, function(req, res) {
     Vocabulary.getlists({"_id":ObjectId(req.query.id)},0,1).then(function(vocabulary){
      if(vocabulary.length == 1){
        var data = {
          title: "Vocabulary",
          layout: 'backend/admin',
          vocabulary:vocabulary[0],
          control:{'level1':'vocabulary'}
        };
        res.render('backend/vocabulary/edit', data);
      }else{
        res.redirect('/admin/vocabulary')
      }
    });  
  });

  /*
  Xóa từ vựng sau đó quay trở trang quản lý từ vưng
   */
  app.get('/admin/vocabulary/delete',isAdmin, function(req, res) {
    Vocabulary.delete({"_id":ObjectId(req.query.id)}).then(function(){
      res.redirect('/admin/vocabulary')
    });
  }); 
    /*
  Live search voca
   */
  
  app.get('/admin/vocabulary/get_livesearch',isAdmin, function(req, res) {
    // res.send('Username: ' + req.query.str_send);
    Vocabulary.getlist_voca(req.query.str_send).then(function(vocabulary){
      var data_respon = '<select id='+'\"'+ 'idparent'+'\"' + ' name='+'\"'+'idparent'+'\"'+'class='+'\"'+'selectpicker'+'\"'+' data-live-search='+'\"'+'true'+'\"'+' data-show-subtext='+'\"'+"true"+'\"'+'>'
      for(var i=0 ; i<vocabulary.length ; i++ ){
        data_respon += '<option value='+'\"'+vocabulary[i].id+'\"'+' data-subtext='+'\"'+vocabulary[i].keyword+'\"'+'>'+vocabulary[i].keyword+'</option>';
      }
      data_respon += "</select>";
      res.json(data_respon);
    });
  }); 
  // app.get('/vocabulary/search',isLoggedIn,function(req, res) {
    
  // });

  /////////////////////////////////////
  // Quản lý calendar cho vocabulary //
  /////////////////////////////////////
  
  /*
  Hiển thị form add lịch từ vừng của ngày hiện tại và bảng hiển thị các ngày đã add lịch cho từng ngày
   */
  app.get('/admin/vocabulary/calendar',isAdmin, function(req, res) {
    var date = get_date_now();
    Vocabulary_config.getlists_2({},0,1000).then(function(vocabulary_config){
      Vocabulary.getlists({},0,1000).then(function(vocabulary){
        Day_topic.getlists({},0,1000).then(function(day_tp){
          var data = {
            title: "Vocabulary",
            layout: 'backend/admin',
            control:{'level1':'vocabulary'},
            vocabulary_config: vocabulary_config,
            vocabulary:vocabulary,
            day_tp    :day_tp,
            day_current:date
          };
          res.render('backend/vocabulary/calendar', data);
        });
      });
    });
  });

  /*
  Hiển thị form edit lịch các từ vựng học của một ngày 
   */
  app.get('/admin/vocabulary/calendar/edit',isAdmin, function(req, res) {
    Vocabulary_config.getlists_2({"_id":ObjectId(req.query.id)},0,1000).then(function(vocabulary_config){
      Vocabulary.getlists({},0,1000).then(function(vocabulary){
        var data = {
          title: "Edit Vocabulary Calendar",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
          vocabulary_config: vocabulary_config[0],
          vocabulary:vocabulary
        };
        res.render('backend/vocabulary/calendar_edit', data);
      });
    });
  });

  /////////////////////////////////////
  // Quản lý câu hỏi cho vocabulary ///
  /////////////////////////////////////
  
  /*
  Hiển thị các list câu hỏi của voca
   */
  app.get('/admin/vocabulary/exercises',isAdmin, function(req, res) {
      Exercise.getlist_voca('voca').then(function(vocabulary){
        var data = {
          title: "List Vocabulary Exercises",
          layout: 'backend/admin',
          control:{'level1':'vocabulary_excise'},
          exercises:vocabulary
        };
        res.render('backend/vocabulary/exercises', data);
      });
  });

  /*
  Hiển thị form add thêm câu hỏi của voca
   */
  app.get('/admin/vocabulary/exercises/add',isAdmin, function(req, res) {
    Vocabulary.getlists({},0,1000).then(function(vocabulary){
        var data = {
          title: "Add Vocabulary Exercise",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
          vocabulary:vocabulary
        };
        res.render('backend/vocabulary/exercise_add', data);
    });
  });

  /*
  Xóa câu hỏi phần vocabulary
   */
  app.get('/admin/vocabulary/exercises/delete',isAdmin, function(req, res) {
    Exercise.delete({"_id":ObjectId(req.query.id)}).then(function(){
      res.redirect('/admin/vocabulary/exercises')
    });
  });

  /*
  Edit câu hỏi phần voca 4
   */
  app.get('/admin/vocabulary/exercises/edit',isAdmin, function(req, res) {
    Exercise.getlists({"_id":ObjectId(req.query.id)},0,1).then(function(exercises){
      Vocabulary.getlists({},0,1000).then(function(vocabulary){
        var data = 
        {
          title: "Edit Vocabulary Exercise",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
          exercises:exercises,
          vocabulary:vocabulary
        };
        res.render('backend/vocabulary/exrcise_edit', data);
      });
    });
  });
  ////////////////////////////////
  // Chủ điểm ngày ///////////////
  ////////////////////////////////
  /*
  List chủ điểm ngày
   */
  app.get('/admin/vocabulary/day',isAdmin, function(req, res) {
    Vocabulary_day.getlists({},0,14).then(function(vocabulary){
      var data = 
        {
          title: "List Vocabulary day",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
          Vocabulary_day:vocabulary,
        };
      res.render('backend/vocabulary/day', data);
    });
  });
  /**
  * Thêm chủ điểm ngày luôn
  */
  app.get('/admin/vocabulary/day/add',isAdmin, function(req, res) {
    Vocabulary_day.getlists({"_id":ObjectId(req.query.id)},0,1).then(function(vocabulary){
      var data = 
        {
          title: "Edit Vocabulary Exercise",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
          vocabulary:vocabulary
        };
      res.render('backend/vocabulary/day', data);
    });
  });
  /**
   * Edit chủ điểm ngày
   */
  app.get('/admin/vocabulary/day/edit',isAdmin, function(req, res) {
    Vocabulary_day.getlists({"_id":ObjectId(req.query.id)},0,14).then(function(exercises){
      var data = 
        {
          title: "Edit Vocabulary Exercise",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
          exercises:exercises,
          vocabulary:vocabulary
        };
      res.render('backend/vocabulary/day', data);
    });
  });
  ////////////////////////////////
  // Chủ điểm tuần ///////////////
  ////////////////////////////////
  /*
  List chủ điểm tuần
   */
  app.get('/admin/vocabulary/week',isAdmin, function(req, res) {
    Vocabulary_week.getlists({},0,14).then(function(vocabulary){
      var data = 
        {
          title: "List Vocabulary week",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
          Vocabulary_week:vocabulary,
        };
      res.render('backend/vocabulary/week', data);
    });
  });
  /**
  * Thêm chủ điểm tuần luôn
  */
  app.get('/admin/vocabulary/week/add',isAdmin, function(req, res) {
      var data = 
        {
          title: "Add Week Vocabulary ",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
        };
      res.render('backend/vocabulary/week_add', data);
  });
  /**
   * Edit chủ điểm tuần
   */
  app.get('/admin/vocabulary/week/edit',isAdmin, function(req, res) {
    Vocabulary_week.getlists({"_id":ObjectId(req.query.id)},0,14).then(function(exercises){
      var data = 
        {
          title: "Edit Vocabulary Exercise",
          layout: 'backend/admin',
          control:{'level1':'vocabulary'},
          exercises:exercises,
          vocabulary:vocabulary
        };
      res.render('backend/vocabulary/week_edit', data);
    });
  });
  /**
   * Xử lý excel
   */
  app.get('/admin/vocabulary/excel', function(req, res) {
    var first_sheet_name = workbook.SheetNames[0];
    var address_of_cell = 'B3';
    var worksheet = workbook.Sheets[first_sheet_name];

    var count = 0;
    var i;

    for (i in worksheet) {
        if (worksheet.hasOwnProperty(i)) {
            count++;
        }
    }
    console.log(count);
    for (var i = 3; i < 60; i++) {
        var address_of_cell_keyword  = 'A'+ i;
        var address_of_cell_phonetic = 'B'+ i;
        var address_of_cell_keyword = 'C'+ i;
        var address_of_cell_keyword = 'D'+ i;
        var address_of_cell_keyword = 'E'+ i;
        var address_of_cell_keyword = 'F'+ i;
        var address_of_cell_keyword = 'G'+ i;
        var address_of_cell_keyword = 'H'+ i;
        var address_of_cell_keyword = 'I'+ i;
        var address_of_cell_keyword = 'J'+ i;

        worksheet[address_of_cell].v
    }
    // if (typeof worksheet[address_of_cell]) {}
    res.send(worksheet[address_of_cell].v);
    
  });
  /***************************************************************** Xử lý **********************************************************************************/
  ////////////////////////////////////////////////////////
  // Xử các request từ các form của hệ thống vocabulary //
  ////////////////////////////////////////////////////////
  /**
   * [switch case từ các thẻ input hidden name type của trang view]
   * <input type="hidden" name="type" value=""/> =>>>>>>>
   */
  app.post('/admin/vocabulary', multipartMiddleware,isAdmin,function(req, res) {
  	//=>>>>>>>RUN
    switch(req.body.type) {
      //Nếu type="add" 
      case 'add':
      	/*---------------Thêm từ vựng nhớ bật view lên nhé----------------------*/
        /*----------------Upload file tương ứng với từ vựng ---------------------*/
        var file = req.files.file;
        // Tên file
        var originalFilename = file.name;
        // File type
        var fileType         = file.type.split('/')[1];
        // File size
        var fileSize         = file.size;
        // Đường dẫn lưu ảnh
        var pathUpload       = 'public/uploads/vocabulary/image/' + originalFilename;

        var file_audio              = req.files.audio;
        var originalFilename_audio  = file_audio.name;
        var fileType_audio          = file_audio.type.split('/')[1];
        var pathUpload_audio        = 'public/uploads/vocabulary/audio/' + originalFilename_audio;
        // Đọc nội dung file tmp
        // nếu không có lỗi thì ghi file vào ổ cứng
        fs.readFile(file.path, function(err, data) {
          if(!err) {
            fs.writeFile(pathUpload, data, function() {
              //repeat callback
                 fs.readFile(file_audio.path, function(err, dataaudio) {
                    if(!err) {
                        fs.writeFile(pathUpload_audio, dataaudio, function() {
	                        vocabulary_info = {
	                           	keyword     : req.body.keyword,
	                           	content     : req.body.content,
	                           	phonetic    : req.body.phonetic,
	                           	audio       : 'uploads/vocabulary/audio/' + originalFilename_audio,
	                           	image       : 'uploads/vocabulary/image/' + originalFilename,
	                           	timeCreated : Math.floor(d.getTime()/1000),
	                           	timeModified: Math.floor(d.getTime()/1000),
	                           	usercreated :"admin"
	                        };
                          	Vocabulary.create(vocabulary_info).then(function(vocabulary){
	                            Vocabulary.update({"_id":ObjectId(vocabulary._id)},{"id":""+vocabulary._id},{upsert:true}).then(function(){
	                              res.redirect('/admin/vocabulary/')
	                          });
                          });
                      });
                    }
                });
            });
          }
        });
    break;
    //Nên tách ra làm 3 vì nếu ifelse callback nhiều rất rồi mắt
		//////////////////////////////
		//Edit một từ gồm 3 loại :  //
		//////////////////////////////
		
      //Edit thông tin cơ bản
    	case 'edit_basic':
		 	vocabulary_info = {
              keyword     : req.body.keyword,
              content     : req.body.content,
              phonetic    : req.body.phonetic,
              timeModified: Math.floor(d.getTime()/1000),
              usercreated :"admin"
          };
          Vocabulary.update({"_id":ObjectId(req.body.id)},vocabulary_info,{upsert:true}).then(function(vocabulary){
              res.redirect('/admin/vocabulary/')
        	});
      break;
  		//Edit hình ảnh của từ vựng
  		case 'edit_image':
  			if (req.files.file.name !== '') {
		        var file = req.files.file;
		        var originalFilename = file.name;
		        var fileType         = file.type.split('/')[1];
		        var fileSize         = file.size;
		        var pathUpload       = 'public/uploads/vocabulary/image/' + originalFilename;
        	};
        	fs.unlink('public/'+req.body.image_old,function(){
        		fs.readFile(file.path, function(err, data) {
		          	if(!err) {
			            fs.writeFile(pathUpload, data, function() {
	                      	vocabulary_info = {
	                            image       : 'uploads/vocabulary/image/' + originalFilename,
	                            timeModified: Math.floor(d.getTime()/1000),
	                            usercreated :"admin"
	                     	};
	                      	Vocabulary.update({"_id":ObjectId(req.body.id)},vocabulary_info,{upsert:true}).then(function(vocabulary){
	                          res.redirect('/admin/vocabulary/')
	                      	});
			            });
		          	}
	        	});
        	});
	   	break;     
  		//Edit âm thanh của từ vựng
  		case 'edit_audio':
	      	if (req.files.audio.name !== '') {
		        var file_audio              = req.files.audio;
		        var originalFilename_audio  = file_audio.name;
		        var fileType_audio          = file_audio.type.split('/')[1];
		        var pathUpload_audio        = 'public/uploads/vocabulary/audio/' + originalFilename_audio;
        	};
        	fs.unlink('public/'+req.body.audio_old,function(){
	        	fs.readFile(file_audio.path, function(err, dataaudio) {
	                    if(!err) {
	                        fs.writeFile(pathUpload_audio, dataaudio, function() {
	                          	vocabulary_info = {
		                            audio       : 'uploads/vocabulary/audio/' + originalFilename_audio,
		                            timeModified: Math.floor(d.getTime()/1000),
		                            usercreated :"admin"
	                         	 };
	                          	Vocabulary.update({"_id":ObjectId(req.body.id)},vocabulary_info,{upsert:true}).then(function(vocabulary){
	                              res.redirect('/admin/vocabulary/')
	                          	});
	                    });
	                }
	            });
	        });
	      break;
      	/*---------------------------------------------------------End edit từ vựng--------------------------------------------------------------------*/

      	/*-----------------Thêm lịch của từ vựng------------------*/
      	case 'calendar':
        Vocabulary_config.count({"day":req.body.calendar}).then(function(count){
          if(count == 1){
            //Lấy list thông tin của từ vựng thông tin đươc thêm vào lịch học của ngày sau đó cắt chuỗi ra để lấy id của từ vựng của từ đó
            var idvocabulary_1 = req.body.idvocabulary_1.substring(7,31);
            var idvocabulary_2 = req.body.idvocabulary_2.substring(7,31);
            var idvocabulary_3 = req.body.idvocabulary_3.substring(7,31);
            var idvocabulary_4 = req.body.idvocabulary_4.substring(7,31);
            var idvocabulary_5 = req.body.idvocabulary_5.substring(7,31);
            var idvocabulary_6 = req.body.idvocabulary_6.substring(7,31);
            //Loại bỏ các ký tự đặc biệt ???? chỗ này thừa hơi
            var vo1 = req.body.idvocabulary_1.replace(/\r?\n|\r/g,'<br>');
            var vo2 = req.body.idvocabulary_2.replace(/\r?\n|\r/g,'<br>');
            var vo3 = req.body.idvocabulary_3.replace(/\r?\n|\r/g,'<br>');
            var vo4 = req.body.idvocabulary_4.replace(/\r?\n|\r/g,'<br>');
            var vo5 = req.body.idvocabulary_5.replace(/\r?\n|\r/g,'<br>');
            var vo6 = req.body.idvocabulary_6.replace(/\r?\n|\r/g,'<br>');
            vocabulary_config_info = {
              listvocabulary: ''+ vo1 +';'+ vo2 +';'+ vo3 +';'+ vo4 +';' + vo5 +';'+ vo6 + '',
              list_id:'"'+ idvocabulary_1 + ',' + idvocabulary_2 + ',' + idvocabulary_3 + ',' + idvocabulary_4 +','+ idvocabulary_5 + ',' + idvocabulary_6 +'"',
              list_keyword:get_keyword(vo1) + ','+ get_keyword(vo2) + ',' + get_keyword(vo3) + ',' + get_keyword(vo4) + ',' + get_keyword(vo5) + ',' + get_keyword(vo6),
              id_day: req.body.cdtuvn,
              timemodified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Vocabulary_config.update({"day":req.body.calendar},vocabulary_config_info,{upsert:true}).then(function(){
              res.redirect('/admin/vocabulary/calendar');
            });
          }else{
            var idvocabulary_1 = req.body.idvocabulary_1.substring(7,31);
            var idvocabulary_2 = req.body.idvocabulary_2.substring(7,31);
            var idvocabulary_3 = req.body.idvocabulary_3.substring(7,31);
            var idvocabulary_4 = req.body.idvocabulary_4.substring(7,31);
            var idvocabulary_5 = req.body.idvocabulary_5.substring(7,31);
            var idvocabulary_6 = req.body.idvocabulary_6.substring(7,31);
            var vo1 = req.body.idvocabulary_1.replace(/\r?\n|\r/g,'<br>');
            var vo2 = req.body.idvocabulary_2.replace(/\r?\n|\r/g,'<br>');
            var vo3 = req.body.idvocabulary_3.replace(/\r?\n|\r/g,'<br>');
            var vo4 = req.body.idvocabulary_4.replace(/\r?\n|\r/g,'<br>');
            var vo5 = req.body.idvocabulary_5.replace(/\r?\n|\r/g,'<br>');
            var vo6 = req.body.idvocabulary_6.replace(/\r?\n|\r/g,'<br>');
            vocabulary_config_info = {
              day: req.body.calendar,
              listvocabulary: ''+ vo1 +';'+ vo2 +';'+ vo3 +';'+ vo4 +';' + vo5 +';'+ vo6 + '',
              list_id:'"'+ idvocabulary_1 + ',' + idvocabulary_2 + ',' + idvocabulary_3 + ',' + idvocabulary_4 +','+ idvocabulary_5 + ',' + idvocabulary_6 +'"',
              list_keyword:get_keyword(vo1) + ','+ get_keyword(vo2) + ',' + get_keyword(vo3) + ',' + get_keyword(vo4) + ',' + get_keyword(vo5) + ',' + get_keyword(vo6),
              id_day: req.body.cdtuvn,
              timecreated: Math.floor(d.getTime()/1000),
              timemodified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Vocabulary_config.create(vocabulary_config_info).then(function(){
              res.redirect('/admin/vocabulary/calendar')
            });
          }
        });
        break;
        /*-----------------Sửa lịch của từ vựng------------------*/
        case 'calendar_edit':
        Vocabulary_config.count({"day":req.body.calendar}).then(function(count){
          if(count == 1){
            vocabulary_config_info = {
              listvocabulary: ''+ req.body.idvocabulary_0 +';'+ req.body.idvocabulary_1 +';'+ req.body.idvocabulary_2 +';'+ req.body.idvocabulary_3 +';' +req.body.idvocabulary_4 + ';'+req.body.idvocabulary_5,
              timeModified: Math.floor(d.getTime()/1000),
              usercreated:"admin"
            }
            Vocabulary_config.update({"day":req.body.calendar},vocabulary_config_info,{upsert:true}).then(function(){
              res.redirect('/admin/vocabulary/calendar')
            });
          }else{
            res.redirect('/admin/vocabulary/calendar')
          }
        });
        break;
        /**
         * VOCA_exersiceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
         */
        /*
        Thêm câu hỏi voca dạng 1
         */
        case 'exercise_add_1':
          var file = req.files.image_lx;
          // Tên file
          var originalFilename = file.name;
          // File type
          var fileType         = file.type.split('/')[1];
          // File size
          var fileSize         = file.size;
          // Đường dẫn lưu ảnh
          var pathUpload       = 'public/uploads/vocabulary/exercise/image/' + originalFilename;

  
          // Đọc nội dung file tmp
          // nếu không có lỗi thì ghi file vào ổ cứng
          fs.readFile(file.path, function(err, data) {
            if(!err) {
              fs.writeFile(pathUpload, data, function() {
                //repeat callback
                if(!err) {
                  var data_tmp = req.body.answer_1.split(',');
                  vocabulary_ex_info = {
                    image_lx    : 'uploads/vocabulary/exercise/image/' + originalFilename,
                    answer      : data_tmp[1],
                    giaithich   : req.body.giaithich
                  };

                  data_add = {
                    type        : 'voca1',
                    content     : JSON.stringify(vocabulary_ex_info),
                    timecreated : Math.floor(d.getTime()/1000),
                    timemodified: Math.floor(d.getTime()/1000),
                    usercreated :"admin",
                    idparent    :data_tmp[0]
                  }

                  Exercise.create(data_add).then(function(vocabulary){
                    Exercise.update({"_id":ObjectId(vocabulary._id)},{"id":""+vocabulary._id},{upsert:true}).then(function(){
                      res.redirect('/admin/vocabulary/exercises/add')
                    });
                  });
                }
              });
            }
          });
        break;
        /*
        Thêm câu hỏi voca dạng 2
         */
        case 'exercise_add_2':
          var data_tmp = req.body.answer_2.split(',');

          vocabulary_ex_info = {
            dvtc        : req.body.dvtc,
            answer      : data_tmp[1],
            giaithich   : req.body.giaithich1
          };

          data_add = {
            type        : 'voca2',
            content     : JSON.stringify(vocabulary_ex_info),
            timecreated : Math.floor(d.getTime()/1000),
            timemodified: Math.floor(d.getTime()/1000),
            usercreated : "admin",
            idparent    : data_tmp[0]
          }

          Exercise.create(data_add).then(function(vocabulary){
            Exercise.update({"_id":ObjectId(vocabulary._id)},{"id":""+vocabulary._id},{upsert:true}).then(function(){
              res.redirect('/admin/vocabulary/exercises/add')
            });
          });
        break;
        /*
        Thêm câu hỏi voca dạng 3
         */
        case 'exercise_add_3':
          var data_tmp = req.body.answer_3.split(',');

          vocabulary_ex_info = {
            audio       : data_tmp[2],
            answer      : data_tmp[1],
            giaithich   : req.body.giaithich2
          };

          data_add = {
            type        : 'voca3',
            content     : JSON.stringify(vocabulary_ex_info),
            timecreated : Math.floor(d.getTime()/1000),
            timemodified: Math.floor(d.getTime()/1000),
            usercreated : "admin",
            idparent    : data_tmp[0]
          };

          Exercise.create(data_add).then(function(vocabulary){
            Exercise.update({"_id":ObjectId(vocabulary._id)},{"id":""+vocabulary._id},{upsert:true}).then(function(){
              res.redirect('/admin/vocabulary/exercises/add')
            });
          });
        break;
        /*
        Thêm câu hỏi voca dạng 4 :multiple choice ( lựa chọn 1 trong 4 đáp án)
         */
        case 'exercise_add_4':
   
            var file_audio              = req.files.audio_4;
            var originalFilename_audio  = file_audio.name;
            var fileType_audio          = file_audio.type.split('/')[1];
            var pathUpload_audio        = 'public/uploads/vocabulary/exercise/audio/' + exercise._id+'.'+fileType_audio;
            fs.readFile(file_audio.path, function(err, dataaudio) {
              if(!err) {
                fs.writeFile(pathUpload_audio, dataaudio, function() {
                  var file_image              = req.files.image_lx_4;
                  var originalFilename_image  = file_image.name;
                  var fileType_image          = file_image.type.split('/')[1];
                  var pathUpload_image        = 'public/uploads/vocabulary/exercise/image/' + exercise._id+'.'+fileType_image;
                  fs.readFile(file_image.path, function(err, dataimage) {
                    if(!err) {
                      fs.writeFile(pathUpload_image, dataimage, function() {
                        // console.log(fileType_image)
                        if(fileType_image == 'jpeg' ||fileType_image == 'png' || fileType_image == 'jpg' ){
                          var content = {
                            "content":req.body.huy,
                            "option1":req.body.option1,
                            "option2":req.body.option2,
                            "option3":req.body.option3,
                            "option4":req.body.option4,
                            "answer":req.body.answer,
                            "giaithich":req.body.giaithich
                          }
                          exercise_info = {
                            content: JSON.stringify(content),
                            audio       : 'public/uploads/vocabulary/exercise/audio/' + originalFilename_audio,
                            image       : 'public/uploads/vocabulary/exercise/image/' + originalFilename_image,
                            type: "voca4",
					        timecreated: Math.floor(d.getTime()/1000),
					        timemodified: Math.floor(d.getTime()/1000),
					        usercreated:"admin",
					        idparent:req.body.idparent
                          }
                          // console.log(exercise_info)
                          Exercise.update({"_id":ObjectId(exercise._id)},exercise_info,{}).then(function(){
                            res.redirect('/admin/vocabulary/exercises/add')
                          });
                        }else{
                          res.redirect('/admin/vocabulary/exercises/add')
                        }
                      });
                    }
                  });
                });
              }
            });
          

        break;
        ////////////////
        // EDIT VOCA  //
        ////////////////
        case 'edit_voca_ex_4' :
        
       
          // var content = {
          //   "content":req.body.huy,
          //   "option1":req.body.option1,
          //   "option2":req.body.option2,
          //   "option3":req.body.option3,
          //   "option4":req.body.option4,
          //   "answer":req.body.answer,
          //   "giaithich":req.body.giaithich
          // }

          // var exercise_update = {
          //   content: JSON.stringify(content),
          //   timemodified: Math.floor(d.getTime()/1000),
          //   usercreated:"admin",
          //   idparent:req.body.idparent
          // };

          // Exercise.update({"_id":ObjectId(req.body.id)},exercise_update,{upsert:true}).then(function(){
          //   res.redirect('/admin/vocabulary/exercises')
          // });
      	Exercise.count({"_id":ObjectId(req.body.id)}).then(function(count){
          if(count == 1){
	            var content = {
		            "content":req.body.huy,
		            "option1":req.body.option1,
		            "option2":req.body.option2,
		            "option3":req.body.option3,
		            "option4":req.body.option4,
		            "answer":req.body.answer,
		            "giaithich":req.body.giaithich
	          	}
	            var exercise_info = {
	              content: JSON.stringify(content),
	              type: "voca4",
	              timemodified: Math.floor(d.getTime()/1000),
	              idparent:req.body.idparent
	            }
            Exercise.update({"_id":ObjectId(req.body.id)},exercise_info,{upsert:true}).then(function(status){
              	var file_audio              = req.files.audio_4;
		        var originalFilename_audio  = file_audio.name;
		        var fileType_audio          = file_audio.type.split('/')[1];
		        var pathUpload_audio        = 'public/uploads/vocabulary/exercise/audio/'  + req.body.id+'.'+fileType_audio;
              fs.readFile(file_audio.path, function(err, dataaudio) {
                if(!err) {
                  fs.rmdir(pathUpload_audio,function(){
                    fs.writeFile(pathUpload_audio, dataaudio, function() {
                       	var file_image              = req.files.image_lx_4;
				        var originalFilename_image  = file_image.name;
				        var fileType_image          = file_image.type.split('/')[1];
				        var pathUpload_image        = 'public/uploads/vocabulary/exercise/image/' + req.body.id+'.'+fileType_image;
                      fs.readFile(file_image.path, function(err, dataimage) {
                        if(!err) {
                          fs.rmdir(pathUpload_image,function(){
                            fs.writeFile(pathUpload_image, dataimage, function() {
                              // console.log(fileType_image)
                              if(fileType_image == 'jpeg' || fileType_image == 'png' || fileType_image == 'jpg'){
                                var content = {
                                  	"content":req.body.huy,
						            "option1":req.body.option1,
						            "option2":req.body.option2,
						            "option3":req.body.option3,
						            "option4":req.body.option4,
						            "answer":req.body.answer,
						            "giaithich":req.body.giaithich
                                }
                                exercise_info = {
                                  	content: JSON.stringify(content),
                                   	audio       : 'public/uploads/vocabulary/exercise/audio/' + originalFilename_audio,
                            		image       : 'public/uploads/vocabulary/exercise/image/' + originalFilename_image,
                            		timemodified: Math.floor(d.getTime()/1000),
						            usercreated:"admin",
						            idparent:req.body.idparent
                                }
                                Exercise.update({"_id":ObjectId(req.body.id)},exercise_info,{}).then(function(){
                                  res.redirect('/admin/vocabulary/exercises')
                                });
                              }else{
                                res.redirect('/admin/vocabulary/exercises')
                              }
                            });
                          });
                        }
                      });
                      res.redirect('/admin/vocabulary/exercises')
                    });
                  });
                }
              });
            });
          }else{
            res.redirect('/admin/vocabulary/exercises')
          }
        }); 
        break;
        ///////////////
        // Week VOCA //
        ///////////////
        /*Thêm chủ điểm tuần*/
        case 'week_add':
          var content = {
            week:req.body.idweek,
            name_week:req.body.cdt,
            content  :req.body.content,
            timecreated : Math.floor(d.getTime()/1000),
            timemodified: Math.floor(d.getTime()/1000),
            usercreated :"admin"
          };

          Vocabulary_week.create(content).then(function(vocabulary){
            Vocabulary_week.update({"_id":ObjectId(vocabulary._id)},{"id":""+vocabulary._id},{upsert:true}).then(function(){
              res.redirect('/admin/vocabulary/week/add')  
            })
          });
          
        break;
        /*Sửa chủ điểm tuần*/
      default :
        res.write("0");
        res.end();
      break;
    }
  });
};
function isAdmin(req, res, next) {
    return next();
}
