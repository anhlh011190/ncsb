///////////////////////
// Anhlh2  //
///////////////////////
module.exports = {
	//Lấy ra năm - tháng - ngày hiện tại // 2017-03-07
	getdate : function(){
		var d = new Date();
	    var year = d.getFullYear();
	    var month = parseInt(d.getMonth()+1);
	    if(month < 10){
	      month = "0"+month
	    }
	    var day = d.getDate();
	    if(day < 10){
	      day = "0"+day
	    }
	    var date = year+'-'+month+"-"+day;
	    return date;
	},
	getdate_next : function(date){
		var year = parseInt(date.substring(0,4))
		var month = parseInt(date.substring(5,7))
		var day = parseInt(date.substring(8,11))
		if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10){
		    if(day == 31){
		      day = 1;
		      month = month + 1
		    }else{
		      day = day + 1
		    }
		}else{
		    if(month == 12){
		      if(day == 31){
		        day = 1;
		        month = 1
		        year = year + 1
		      }else{
		        day = day + 1
		      }
		    }else{
		      if(month == 4 || month == 6 || month == 9 || month == 11){
		        if(day == 30){
		          day = 1;
		          month = month + 1
		        }else{
		          day = day + 1
		        }
		      }else{
		        if(month == 2){
		          if(day == 28){
		            day = 1;
		            month = month + 1
		          }else{
		            day = day + 1
		          }
		        }
		      }
		    }
		}     
		if(month < 10){
		    month = "0"+month
		}
		if(day < 10){
		  day = "0"+day
		}
		var date = year+'-'+month+"-"+day;
		return date;
	},
}